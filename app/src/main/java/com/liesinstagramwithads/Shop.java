package com.liesinstagramwithads;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.adapter.ShopAdapter;
import com.billing.v3.IabHelper;
import com.billing.v3.IabResult;
import com.billing.v3.Inventory;
import com.billing.v3.Purchase;
import com.getmorelikes.pro.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.liesinstagramwithads.UrlEngine.GetSaveData;
import com.liesinstagramwithads.UrlEngine.PurchaseStateChangedListener;
import com.net.MImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.util.Config.log_tag;

public class Shop extends BaseActivity implements PurchaseStateChangedListener {
    MImageLoader loader;
    ListView list1;
    IabHelper mHelper;
    static final int RC_REQUEST = 10001;
    View view;

    public static String notificationId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // public View onCreateView(LayoutInflater inflater, ViewGroup
        // container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		Tracker app_tracker = ((LB) Shop.this.getApplication()).getTracker(LB.TrackerName.APP_TRACKER);
//		Tracker global_tracker = ((LB) this.getApplication()).getTracker(LB.TrackerName.GLOBAL_TRACKER);
//		app_tracker.setScreenName("Buy coins");
//		app_tracker.send(new HitBuilders.AppViewBuilder().build());
//		global_tracker.send(new HitBuilders.AppViewBuilder().build());
        // view = inflater.inflate(R.layout.activity_shop, container, false);
        setContentView(R.layout.activity_shop);


        //check if possible server update error after purchase reload
        UrlEngine.upgradeUser(this, "1496142301", "test_update", new GetSaveData() {
            @Override
            public void OnCompletion(JSONObject json) {
                Log.d(log_tag, "setOnTouchListener json payment:" + json);
                try {
                    if (!json.get("operation").equals("ok")) {
                        doDialog();
                    } else {

                    }
                } catch (JSONException e) {
                    doDialog();
                    e.printStackTrace();
                }
                // coins.setText(LB.getInstagramUser().getUserPoints());
            }

            @Override
            public void comple(String json) {
                Log.d(log_tag, "setOnTouchListener json paymen2t:" + json);
                // coins.setText(LB.getInstagramUser().getUserPoints());
            }
        });
        list1 = (ListView) findViewById(R.id.listView1);

        ArrayList<String> data = new ArrayList<String>();
        data.add("testing");
        data.add("testing");
        data.add("testing");
        data.add("testing");
        data.add("testing");
        // String base64EncodedPublicKey =
        // "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAndaOybLc1yDEwmbsN+8HTQyBTrBToO3O9ZDFS0onwhGoBJcWF+c2na/lwB/QG1CEy1MDKBkscvigr52IRhXqRSfZ26EerTXRrF93WAVulcHaqEgsi1VYlNVTG6hiDfYJeOpc36t+6D2z9YsVGFTLD+ulA95m/IGvuMzDka73PGE+w/YBIb2swXupBDzs95RrGX7SvHkyeo4RwMWZSTr8g9pNlyfNPvFN5McG54MC3z3281aVkNsr8L+smqEr0heqFjxabVwDKjU/eLgLAzPB37G6xVt0Pdd9X7V0qWurME7vNMk+tAmJhR5RD/xDxvkPjEb9aF8CdAgFMZnB0W/VewIDAQAB";
        String base64EncodedPublicKey = com.util.Config.encodedPublicKey;
        // Create the helper, passing it our context and the public key to
        // verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(Shop.this, base64EncodedPublicKey);
        // enable debug logging (for a production application, you should set
        // this to false).
        mHelper.enableDebugLogging(true);
        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    // complain("Problem setting up in-app billing: " + result);
                    return;
                }
                // Hooray, IAB is fully set up. Now, let's get an inventory of
                // stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
        ShopAdapter adapter = new ShopAdapter(Shop.this, data, mHelper, mPurchaseFinishedListener);
        list1.setAdapter(adapter);
        // return view;
    }

    // @Override
    // public void onAttach(Activity activity) {
    // super.onAttach(activity);
    // Log.d(log_tag,"onAttach");
    // }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    public void doDialog() {
        AlertDialog ad = new AlertDialog.Builder(this)

                // Set Dialog Title
                .setTitle("Server error")
                // Set Dialog Message
                .setMessage("Click ok to retry")

                // Positive button
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Do something else
                        Log.d(log_tag, "clicked ok");
                        Intent intent = new Intent(Shop.this, Shop.class);
                        startActivity(intent);
                    }
                })

                // Negative Button
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(log_tag, "clicked cancel");
                        Intent intent = new Intent(Shop.this, MainActivity.class);
                        startActivity(intent);
                        // Do something else
                    }
                }).create();
        ad.show();
    }

    private String TAG = "TAG";

    @Override
    public void onStart() {
        super.onStart();
        Log.d(log_tag, "onstart shop analitics");
        // Get an Analytics tracker to report app starts & uncaught exceptions
        // etc.
        // GoogleAnalytics.getInstance(this).reportActivityStart(this);
        GoogleAnalytics.getInstance(Shop.this.getApplicationContext()).reportActivityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        // Stop the analytics tracking
        // GoogleAnalytics.getInstance(this).reportActivityStop(this);
        GoogleAnalytics.getInstance(Shop.this.getApplicationContext()).reportActivityStop(this);
    }

    TextView coins;

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences("ig-user", 0);
        Editor editor11 = sharedPrefs.edit();
        editor11.putString("likes", LB.getInstagramUser().getUserPoints() + "");
        editor11.commit();
        // registerForContextMenu(view);
        if (Shop.this != null)
            Shop.this.invalidateOptionsMenu();
        // coins = (TextView) findViewById(R.id.coins);
        //
        // coins.setText(LB.getInstagramUser().getUserPoints() + "");
    }

    ;

    // public static void ShopDialog(final Activity act) {
    // Log.d(log_tag,"in ShopDialog");
    // AlertDialog.Builder builder = new AlertDialog.Builder(act);
    //
    // builder.setTitle("Get more likes");
    // builder.setMessage("Visit our shop to get more coins and boost your likes");
    //
    // builder.setPositiveButton("Get likes", new
    // DialogInterface.OnClickListener() {
    //
    // public void onClick(DialogInterface dialog, int which) {
    // // Do nothing but close the dialog
    // // Intent intent = new Intent(act, Shop.class);
    // // act.startActivity(intent);
    //
    // Fragment fragment = null;
    // fragment = new Shop();
    // FragmentManager fragmentManager = act.getFragmentManager();
    // //
    // fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.frame_container,
    // // fragment).commit();
    //
    // if (fragmentManager != null) {
    // FragmentTransaction ft = fragmentManager.beginTransaction();
    //
    // ft.addToBackStack(null);
    //
    // ft.replace(R.id.frame_container, fragment);
    // ft.commit();
    // //
    // Pay.this.getActivity().getFragmentManager().beginTransaction().replace(R.id.frame_container,
    // }
    // dialog.dismiss();
    // }
    //
    // });
    //
    // builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
    // {
    //
    // @Override
    // public void onClick(DialogInterface dialog, int which) {
    // // Do nothing
    // dialog.dismiss();
    // }
    // });
    //
    // AlertDialog alert = builder.create();
    // alert.show();
    // }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, MainActivity.class);
        //
        startActivity(intent);
    }

    @Override
    public void onPurchaseStateChanged() {

    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            if (result.isFailure()) {
                if (result.getResponse() == IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED)
                    return;
            }
            Log.d(TAG, "Purchase successful.");
            mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            UrlEngine.upgradeUser(Shop.this, LB.getInstagramUser().id, purchase.getSku(), new GetSaveData() {
                @Override
                public void OnCompletion(JSONObject json) {
                    Log.d(log_tag, "json payment:" + json);
                    // coins.setText(LB.getInstagramUser().getUserPoints());
                }

                @Override
                public void comple(String json) {
                    Log.d(log_tag, "json paymen2t:" + json);
                    // coins.setText(LB.getInstagramUser().getUserPoints());
                }
            });
        }
    };
    // Listener that's called when we finish querying the items and
    // subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");
            if (result.isFailure()) {
                return;
            }
            if (inventory.hasPurchase("c_1")) {
                mHelper.consumeAsync(inventory.getPurchase("c_1"), null);
            } else if (inventory.hasPurchase("c_2")) {
                mHelper.consumeAsync(inventory.getPurchase("c_2"), null);
            } else if (inventory.hasPurchase("c_3")) {
                mHelper.consumeAsync(inventory.getPurchase("c_3"), null);
            } else if (inventory.hasPurchase("c_4")) {
                mHelper.consumeAsync(inventory.getPurchase("c_4"), null);
            } else if (inventory.hasPurchase("c_5")) {

                mHelper.consumeAsync(inventory.getPurchase("c_5"), null);

            }

            Log.d(TAG, "Query inventory was successful.");

			/*
             * Check for items we own. Notice that for each purchase, we check
			 * the developer payload to see if it's correct! See
			 * verifyDeveloperPayload().
			 */

            // Do we have the premium upgrade?

            Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);
            // We know this is the "gas" sku because it's the only one we
            // consume,
            // so we don't check which sku was consumed. If you have more than
            // one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in
                // our
                // game world's logic, which in our case means filling the gas
                // tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");
            } else {
            }
            Log.d(TAG, "End consumption flow.");
        }
    };
}
