package com.liesinstagramwithads;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.getmorelikes.pro.R;

public class HelpActivity extends Activity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedpreferences = this.getApplicationContext().getSharedPreferences("ig-user", 0);
        String help = sharedpreferences.getString("help", "not");
        if (help.equals("done")) {
//			Log.d("tag","one");
            finish();
            goToLogin();
        }

        // Create the view switcher
        HorizontalPager realViewSwitcher = new HorizontalPager(getApplicationContext());

        // Add some views to it
        final int[] backgroundColors = {Color.RED, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW};
        for (int i = 0; i < 5; i++) {
            TextView textView = new TextView(getApplicationContext());
            textView.setText(Integer.toString(i + 1));
            textView.setTextSize(100);
            textView.setTextColor(Color.BLACK);
            textView.setGravity(Gravity.CENTER);
            textView.setBackgroundColor(backgroundColors[i]);

            // realViewSwitcher.addView(textView);

            ImageView imageView = new ImageView(this);
            // setting image resource
            if (i == 0)
                imageView.setImageResource(R.drawable.step11);
            if (i == 1)
                imageView.setImageResource(R.drawable.step22);
            if (i == 2)
                imageView.setImageResource(R.drawable.step33);
            // setting image position
            imageView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

            // adding view to layout
            realViewSwitcher.addView(imageView);
        }

        // set as content view
        setContentView(R.layout.help_layout);
        HorizontalPager realViewSwitcher1 = (HorizontalPager) findViewById(R.id.real_view_switcher);
        TextView skip = (TextView) findViewById(R.id.skip);
        // Yeah, it really is as simple as this :-)

		/*
         * Note that you can also define your own views directly in a resource
		 * XML, too by using:
		 * <com.github.ysamlan.horizontalpager.RealViewSwitcher
		 * android:layout_width="fill_parent"
		 * android:layout_height="fill_parent"
		 * android:id="@+id/real_view_switcher"> <!-- your views here -->
		 * </com.github.ysamlan.horizontalpager.RealViewSwitcher>
		 */

        // OPTIONAL: listen for screen changes
        realViewSwitcher1.setOnScreenSwitchListener(onScreenSwitchListener);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLogin();
            }
        });
    }

    private void goToLogin() {
        android.content.SharedPreferences.Editor editor = this.getSharedPreferences("ig-user", 0).edit();
        editor.putString("help", "done");
        editor.commit();

        Intent intent = new Intent(HelpActivity.this, Login.class);
        startActivity(intent);

    }

    private final HorizontalPager.OnScreenSwitchListener onScreenSwitchListener = new HorizontalPager.OnScreenSwitchListener() {
        @Override
        public void onScreenSwitched(final int screen) {
			/*
			 * this method is executed if a screen has been activated, i.e. the
			 * screen is completely visible and the animation has stopped (might
			 * be useful for removing / adding new views)
			 */
            //Log.d("HorizontalPager", "switched to screen: " + screen);
            if (screen == 3) {
                goToLogin();
            }
        }
    };
}
