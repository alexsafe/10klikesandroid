package com.liesinstagramwithads;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import com.adapter.NavDrawerListAdapter;
import com.billing.v3.IabHelper;
import com.datasets.ItemBatch;
import com.datasets.NavDrawerItem;
import com.getmorelikes.pro.R;
import com.liesinstagramwithads.UrlEngine.GetCompletionListener;
import com.liesinstagramwithads.UrlEngine.GetSaveData;
import com.net.InstagramApp;
import com.util.ConnectionDetector;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.util.Config.log_tag;

@SuppressLint("NewApi")
public class MainActivity extends BaseActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;
    ConnectionDetector cd;
    // used to store app title
    private CharSequence mTitle;
    IabHelper mHelper;
    String newString = "ini";
    // slide menu items
    private String[] navMenuTitles;
    // private TypedArray navMenuIcons;
    ArrayList<ItemBatch> itemes = new ArrayList<ItemBatch>();
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    int index = 0;
    InstagramApp mApp;
    private boolean doubleBackToExitPressedOnce;

    private void loadCoins1(final Menu menu) {
        // menu.findItem(R.id.noOfCoins).setTitle(LB.getInstagramUser().getUserPoints()
        // + "");
        // menu.findItem(R.id.noOfCoins).setTitle("400");
    }

    private void loadCoins2() {
        Log.d(log_tag, "invalidate load coins");
        UrlEngine.getItemBatch(MainActivity.this, LB.getInstagramUser().id, new GetCompletionListener() {
            @Override
            public void OnCompletion(ArrayList<ItemBatch> data) {

            }
        });
    }

    private void loadCoins() {
        Log.d(log_tag, "invalidate load coins");
        UrlEngine.getItemBatch(MainActivity.this, LB.getInstagramUser().id, new GetCompletionListener() {
            @Override
            public void OnCompletion(ArrayList<ItemBatch> data) {
                Log.d(log_tag, "invalidate load coins completion");
                if (data == null) {

                    // final AlertDialog ad = new
                    // AlertDialog.Builder(MainActivity.this)
                    //
                    // // Set Dialog Title
                    // .setTitle("Server timeout")
                    // // Set Dialog Message
                    // .setMessage("Click ok to retry loading correct number of coins")
                    //
                    // // Positive button
                    // .setPositiveButton("OK", new
                    // DialogInterface.OnClickListener() {
                    // public void onClick(DialogInterface dialog, int which) {
                    // // Do something else
                    // Log.d(log_tag, "clicked ok");
                    // loadCoins(menu);
                    // }
                    // })
                    //
                    // // Negative Button
                    // .setNegativeButton("Cancel", new
                    // DialogInterface.OnClickListener() {
                    // public void onClick(DialogInterface dialog, int which) {
                    // Log.d(log_tag, "clicked cancel");
                    // // Do something else
                    //
                    // }
                    // }).create();
                    // ad.show();
                    // menu.findItem(R.id.noOfCoins).setTitle(LB.getInstagramUser().getUserPoints()
                    // + "");
                    if (MainActivity.this != null)
                        MainActivity.this.invalidateOptionsMenu();
                } else {
                    itemes = new ArrayList<ItemBatch>();
                    itemes.addAll(data);
                    Log.d(log_tag, "invalidate items in complete: " + itemes.size());
                    Log.d(log_tag, "invalidate items in complete: " + itemes);
                    if (itemes.size() > 0)
                        if (itemes.get(index).campaign_id != null)
                            UrlEngine.getStats(MainActivity.this, LB.getInstagramUser().id, itemes.get(index).campaign_id, "0", new GetSaveData() {

                                @Override
                                public void OnCompletion(JSONObject json) {
                                    Log.d(log_tag, " invalidate on complete:" + json);

                                    // coins.setText(LB.getInstagramUser().getUserPoints()
                                    // +
                                    // "");
                                    Log.d(log_tag, "invalidate load coins:" + LB.getInstagramUser().getUserPoints());
                                    if (MainActivity.this != null)
                                        MainActivity.this.invalidateOptionsMenu();
                                    // menu.findItem(R.id.noOfCoins).setTitle("300");
                                    // coins.setText("10000");
                                }

                                @Override
                                public void comple(String json) {
                                    Log.d(log_tag, "on comple");
                                    // coins.setText(LB.getInstagramUser().getUserPoints()
                                    // +
                                    // "");
                                }
                            }, true);
                }
            }
        });
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Log.d(log_tag, "oncreate main:" + savedInstanceState);

        // Tracker t = ((LB)
        // this.getApplication()).getTracker(TrackerName.APP_TRACKER);
        // t.setScreenName("Main Activity");
        // t.send(new HitBuilders.AppViewBuilder().build());

        // Log.d(log_tag, "on create main 1");
        checkConn();
        // getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_main);
        Log.d(log_tag, "oncreate main:" + newString);
        loadCoins2();

        Bundle extras;
        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                newString = null;
            } else {
                newString = extras.getString("screen");
            }
        } else {
            newString = (String) savedInstanceState.getSerializable("screen");
        }
        Log.d(log_tag, "newString inainte:" + newString);
        Fragment fragment = null;
        if (newString != null) {

            if (newString.equals("selectPhotos") || newString.equals("ini")) {
                Log.d(log_tag, "newString if:" + newString);
                fragment = new SelectPhoto();
            }
            if (newString.equals("Rate")) {
                fragment = new Rate();
            }
        } else {
            fragment = new SelectPhoto();
        }
        FragmentManager fragmentManager = getFragmentManager();
        // fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.frame_container,
        // fragment).commit();

        if (fragmentManager != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();

            ft.addToBackStack(null);

            ft.replace(R.id.frame_container_base, fragment);
            ft.commit();
            // Pay.this.getActivity().getFragmentManager().beginTransaction().replace(R.id.frame_container,

        }

        // getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bg));

        UrlEngine.getAppData(MainActivity.this);

        SharedPreferences sharedpreferences = this.getApplicationContext().getSharedPreferences("ig-user", 0);
        String access_token = sharedpreferences.getString("access_token", null);
        // android.content.SharedPreferences.Editor editor =
        // sharedpreferences.edit();
        // editor.putInt("rated", 0);
        // editor.commit();
        String NActions = sharedpreferences.getString("NActions", null);
        int rated = sharedpreferences.getInt("rated", 0);

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            // displayView(0);
        }
        // loadCoins(getActionBar().);

    }

    public void retry(View v) {
        Log.d(log_tag, "retry clicked");
    }

    // @Override
    // public void onBackPressed() {
    // final Pay fragment = (Pay)
    // getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);
    //
    // if (fragment.allowBackPressed()) { // and then you define a method
    // // allowBackPressed with the logic
    // // to allow back pressed or not
    // super.onBackPressed();
    // }
    // }

    public void checkConn() {
        Log.d(log_tag, "check internet connection");
        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Internet Connection Error");
            alertDialogBuilder.setMessage("Please connect to working Internet connection").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    MainActivity.this.finish();
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return;
        }
    }

    private static long back_pressed;

    @Override
    public void onResume() {
        super.onResume();
        Log.d(log_tag, "main onresume");
    }

    @Override
    public void onBackPressed() {
        Log.d(log_tag, "onBackPressed");
        Log.d(log_tag, "onBackPressed getFragmentManager().getBackStackEntryCount():" + getFragmentManager().getBackStackEntryCount());

        if (getFragmentManager().getBackStackEntryCount() <= 1) {

            this.finish();
        } else {

            // super.onBackPressed();
            getFragmentManager().popBackStack();
        }
        // if (back_pressed + 2000 > System.currentTimeMillis()) {
        // // super.onBackPressed();
        // this.finish();
        // } else {
        // Toast.makeText(getBaseContext(), "Press once again to exit!",
        // Toast.LENGTH_SHORT).show();
        // back_pressed = System.currentTimeMillis();
        // }
    }

}