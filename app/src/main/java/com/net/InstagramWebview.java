package com.net;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.getmorelikes.pro.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.liesinstagramwithads.LB;

import static com.util.Config.log_tag;

public class InstagramWebview extends Activity {

    GoogleAnalytics mTrackers;
    protected LB GetLikesOnInstagram;
    private InstagramApp mApp;
    private ProgressDialog mSpinner;
    // private OAuthDialogListener mListener;
    Context context;
    String URL = "https://instagram.com/accounts/login/?force_classic_login=&next=/oauth/authorize/%3Fclient_id%3D0508a0f6eb2e4eecae660e52a659a884%26redirect_uri%3Dhttp%3A//soma.greenlab.ro/10000likespro/callback_uri.php%26response_type%3Dcode%26display%3Dtouch%26scope%3Dlikes%2Bcomments%2Brelationships";
    String TAG = "tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(log_tag, "create login webview");
        super.onCreate(savedInstanceState);
        mSpinner = new ProgressDialog(this);
        mSpinner.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mSpinner.setMessage("Loading...");
        setContentView(R.layout.webview);
        // final WebView learn2crack = (WebView) findViewById(R.id.webview);
        // learn2crack
        // .loadUrl("https://instagram.com/accounts/login/?force_classic_login=&next=/oauth/authorize/%3Fclient_id%3D0508a0f6eb2e4eecae660e52a659a884%26redirect_uri%3Dhttp%3A//soma.greenlab.ro/10000likespro/callback_uri.php%26response_type%3Dcode%26display%3Dtouch%26scope%3Dlikes%2Bcomments%2Brelationships");
        // learn2crack.getSettings().setJavaScriptEnabled(true);

        WebView webView = (WebView) findViewById(R.id.webview);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        WebViewClientImpl webViewClient = new WebViewClientImpl(this);
        webView.setWebViewClient(webViewClient);

        webView.loadUrl(URL);
    }

    public class WebViewClientImpl extends WebViewClient {

        private Activity activity = null;

        public WebViewClientImpl(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(TAG, "Redirecting URL " + url);

            if (url.startsWith(InstagramApp.mCallbackUrl)) {
                String urls[] = url.split("=");
                // mListener.onComplete(urls[1]);
                mSpinner.dismiss();
                return true;
            }
            return false;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Log.d(TAG, "Page error: " + description);

            super.onReceivedError(view, errorCode, description, failingUrl);
            // mListener.onError(description);
            mSpinner.dismiss();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.d(TAG, "Loading URL: " + url);

            super.onPageStarted(view, url, favicon);
            mSpinner.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            // String title = mWebView.getTitle();
            // if (title != null && title.length() > 0) {
            // mTitle.setText(title);
            // }
            Log.d(TAG, "onPageFinished URL: " + url);
            mSpinner.dismiss();
        }

    }
}