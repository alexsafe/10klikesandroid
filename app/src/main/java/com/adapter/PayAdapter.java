package com.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.datasets.ItemBatch;
import com.getmorelikes.pro.R;
import com.liesinstagramwithads.LB;
import com.liesinstagramwithads.ShareFragment;
import com.liesinstagramwithads.UrlEngine;
import com.liesinstagramwithads.UrlEngine.GetCompletionListener;
import com.liesinstagramwithads.UrlEngine.GetSaveData;
import com.util.AppRater;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.util.Config.log_tag;

public class PayAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    int target = 0;
    ArrayList<String> data = new ArrayList<String>();
    String mediaId;
    String media_url;
    Activity activity;
    TextView mCoins;
    View vi;
    SharedPreferences sharedpreferences;

    public PayAdapter(Activity activity, ArrayList<String> data, String mediaId, String media_url, TextView mCoins) {
        this.data = data;
        this.media_url = media_url;
        this.mediaId = mediaId;
        this.activity = activity;
        this.mCoins = mCoins;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * @param convertView The old view to overwrite, if one is passed
     * @returns a ContactEntryView that holds wraps around an ContactEntry
     */
    @SuppressLint("NewApi")
    public View getView(final int position, View convertView, final ViewGroup parent) {
        vi = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.activity_pay_row, null);
            holder = new ViewHolder();
            Log.d(log_tag, "parent:" + parent.getRootView());
            Log.d(log_tag, "parent convertView:" + convertView);
            holder.likes = (TextView) vi.findViewById(R.id.likesTxt);

            holder.coinButton = (Button) vi.findViewById(R.id.coinButton);

            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (position == 0) {
            // holder.likes.setText("Get 25");
            holder.coinButton.setText(activity.getApplicationContext().getResources().getString(R.string.plus_1_like));
        } else if (position == 1) {
            // holder.likes.setText("Get 50");
            holder.coinButton.setText(activity.getApplicationContext().getResources().getString(R.string.plus_10_like));

        } else if (position == 2) {
            // holder.likes.setText("Get 100");
            holder.coinButton.setText(activity.getApplicationContext().getResources().getString(R.string.plus_25_like));

        } else if (position == 3) {
            // holder.likes.setText("Get 250");
            holder.coinButton.setText(activity.getApplicationContext().getResources().getString(R.string.get_more_coins));

        }

        sharedpreferences = activity.getApplicationContext().getSharedPreferences("ig-user", 0);
        holder.coinButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                int coins = 0;
                if (position == 0) {
                    coins = 2;
                    target = 1;

//					TextView rowView = (TextView) ((ViewGroup) vi).getChildAt(R.id.noOfLikes);
//					rowView.setText("jjj");

                } else if (position == 1) {
                    coins = 20;
                    target = 10;

                } else if (position == 2) {
                    coins = 50;
                    target = 25;

                } else if (position == 3) {
                    // coins = 50;
                    // target = 100;
//					Intent intentShop = new Intent(Pay.this.getActivity(),Shop.class);
//					Pay.this.getActivity().startActivity(intentShop);
//					Fragment nextFrag = new Shop();
//					FragmentManager fm = activity.getFragmentManager();
//
//					if (fm != null) {
//						FragmentTransaction ft = fm.beginTransaction();
//
//						ft.addToBackStack(null);
//						ft.replace(R.id.frame_container, nextFrag);
//						ft.commit();
                    // Pay.this.getActivity().getFragmentManager().beginTransaction().replace(R.id.frame_container,
                    // nextFrag).addToBackStack(null).commit();
//					}
                }

                if (position < 3) {
                    if (Integer.parseInt(LB.getInstagramUser().getUserPoints()) < coins) {
//						Shop.ShopDialog(activity);
//						Intent intentShop = new Intent(Pay.this,Shop.class);
//						startActivity(intentShop);
                    } else {
                        if (android.os.Build.VERSION.SDK_INT > 9) {
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);
                        }
                        UrlEngine.starCampaign(activity, mediaId, media_url, target + "", coins, LB.getInstagramUser().id, new GetSaveData() {
                            @Override
                            public void OnCompletion(JSONObject json) {
                                UrlEngine.autolike(activity, mediaId, media_url, target);
                                try {
                                    int campaignId = json.getInt("campaign_id");
                                    UrlEngine.addUltraFastLikes(activity, LB.getInstagramUser().id, campaignId + "", target + "", new GetCompletionListener() {
                                        @Override
                                        public void OnCompletion(ArrayList<ItemBatch> data) {
                                            // activity.finish();
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                android.content.SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("noLikes", "nolikes");
                                editor.commit();
                                activity.registerForContextMenu(vi);
                                activity.invalidateOptionsMenu();
                            }

                            @Override
                            public void comple(String json) {
                                UrlEngine.autolike(activity, mediaId, media_url, target);
                                JSONObject jsoner;
                                try {
                                    jsoner = new JSONObject(json);
                                    int campaignId = jsoner.getInt("campaign_id");
                                    UrlEngine.addUltraFastLikes(activity, LB.getInstagramUser().id, campaignId + "", target + "", new GetCompletionListener() {
                                        @Override
                                        public void OnCompletion(ArrayList<ItemBatch> data) {
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
                Log.d(log_tag, "shared prefs in pay:" + sharedpreferences);

                String PModal = sharedpreferences.getString("PModal", null);
                String PShare = sharedpreferences.getString("PShare", null);
                String PRate = sharedpreferences.getString("PRate", null);
                int rated = sharedpreferences.getInt("rated", 80);
                int probModal = Integer.parseInt(PModal);
                int probShare = Integer.parseInt(PShare);
                int probRate = Integer.parseInt(PRate);
                Log.d(log_tag, "shared prefs in pay PRate:" + PRate);
                Log.d(log_tag, "shared prefs in pay rated:" + rated);
                // int probRate = Integer.parseInt(PRate);
                int checkModal = (int) (Math.random() * 100);
//				checkModal=13;
                Fragment nextFrag = new ShareFragment();
                Log.d(log_tag, "probShare: " + probShare + " checkModal:" + checkModal + " probmodal:" + probModal + " probRate:" + probRate);
                if (checkModal < probModal) {
                    int check2 = (int) (Math.random() * 100);
//					check2=60;
                    if (check2 < probShare) {
                        Bundle bundle = new Bundle();
                        bundle.putString("url", media_url);
                        bundle.putString("id", mediaId);
                        bundle.putInt("position", position);
                        // bundle.putString("coins", mCoins);
                        Log.d(log_tag, "bundle: " + bundle);
                        nextFrag.setArguments(bundle);
                        FragmentManager fm = activity.getFragmentManager();
                        if (fm != null) {
                            FragmentTransaction ft = fm.beginTransaction();

                            ft.addToBackStack(null);

                            ft.replace(R.id.frame_container, nextFrag);
                            ft.commit();
                        }
                    } else if (rated != 1) {
                        //if (checkModal < probRate) {
                        AppRater.app_launched(activity);
//						}
                    }
                }
            }
        });

        return vi;
    }

    public static class ViewHolder {
        TextView likes;
        Button coinButton;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

}
