// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

// Referenced classes of package com.film.instaliker.aouth.privateApi.rest:
//            SerializableCookie

public class PersistentCookieStore
        implements CookieStore {

    private static final String COOKIE_NAME_PREFIX = "cookie_";
    private static final String COOKIE_NAME_STORE = "names";
    private static final String COOKIE_PREFS = "CookiePrefsFile";
    private static final String LOG_TAG = "PersistentCookieStore";
    private final SharedPreferences cookiePrefs;
    private final ConcurrentHashMap cookies = new ConcurrentHashMap();

    public PersistentCookieStore(Context context) {
        cookiePrefs = context.getSharedPreferences("CookiePrefsFile", 0);
        String s = cookiePrefs.getString("names", null);
        String as[] = null;
        int i = 0;
        int j = 0;
        if (s != null) {
            as = TextUtils.split(s, ",");
            j = as.length;
        }
        do {
            if (i >= j) {
                clearExpired(new Date());
                return;
            }
            String s1 = as[i];
            String s2 = cookiePrefs.getString((new StringBuilder("cookie_")).append(s1).toString(), null);
            if (s2 != null) {
                Cookie cookie = decodeCookie(s2);
                if (cookie != null) {
                    cookies.put(s1, cookie);
                }
            }
            i++;
        } while (true);
    }

    public void addCookie(Cookie cookie) {
        String s = (new StringBuilder(String.valueOf(cookie.getName()))).append(cookie.getDomain()).toString();
        android.content.SharedPreferences.Editor editor;
        if (!cookie.isExpired(new Date())) {
            cookies.put(s, cookie);
        } else {
            cookies.remove(s);
        }
        editor = cookiePrefs.edit();
        editor.putString("names", TextUtils.join(",", cookies.keySet()));
        editor.putString((new StringBuilder("cookie_")).append(s).toString(), encodeCookie(new SerializableCookie(cookie)));
        editor.commit();
    }

    protected String byteArrayToHexString(byte abyte0[]) {
        StringBuilder stringbuilder = new StringBuilder(2 * abyte0.length);
        int i = abyte0.length;
        int j = 0;
        do {
            if (j >= i) {
                return stringbuilder.toString().toUpperCase(Locale.US);
            }
            int k = 0xff & abyte0[j];
            if (k < 16) {
                stringbuilder.append('0');
            }
            stringbuilder.append(Integer.toHexString(k));
            j++;
        } while (true);
    }

    public void clear() {
        android.content.SharedPreferences.Editor editor = cookiePrefs.edit();
        Iterator iterator = cookies.keySet().iterator();
        do {
            if (!iterator.hasNext()) {
                editor.remove("names");
                editor.commit();
                cookies.clear();
                return;
            }
            String s = (String) iterator.next();
            editor.remove((new StringBuilder("cookie_")).append(s).toString());
        } while (true);
    }

    public boolean clearExpired(Date date) {
        boolean flag = false;
        android.content.SharedPreferences.Editor editor = cookiePrefs.edit();
        Iterator iterator = cookies.entrySet().iterator();
        do {
            java.util.Map.Entry entry;
            String s;
            do {
                if (!iterator.hasNext()) {
                    if (flag) {
                        editor.putString("names", TextUtils.join(",", cookies.keySet()));
                    }
                    editor.commit();
                    return flag;
                }
                entry = (java.util.Map.Entry) iterator.next();
                s = (String) entry.getKey();
            } while (!((Cookie) entry.getValue()).isExpired(date));
            cookies.remove(s);
            editor.remove((new StringBuilder("cookie_")).append(s).toString());
            flag = true;
        } while (true);
    }

    protected Cookie decodeCookie(String s) {
        ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(hexStringToByteArray(s));
        Cookie cookie;
        try {
            cookie = ((SerializableCookie) (new ObjectInputStream(bytearrayinputstream)).readObject()).getCookie();
        } catch (Exception exception) {
            Log.d("PersistentCookieStore", "decodeCookie failed", exception);
            return null;
        }
        return cookie;
    }

    protected String encodeCookie(SerializableCookie serializablecookie) {
        if (serializablecookie == null) {
            return null;
        }
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        String s;
        try {
            (new ObjectOutputStream(bytearrayoutputstream)).writeObject(serializablecookie);
            s = byteArrayToHexString(bytearrayoutputstream.toByteArray());
        } catch (Exception exception) {
            return null;
        }
        return s;
    }

    public List getCookies() {
        return new ArrayList(cookies.values());
    }

    protected byte[] hexStringToByteArray(String s) {
        int i = s.length();
        byte abyte0[] = new byte[i / 2];
        int j = 0;
        do {
            if (j >= i) {
                return abyte0;
            }
            abyte0[j / 2] = (byte) ((Character.digit(s.charAt(j), 16) << 4) + Character.digit(s.charAt(j + 1), 16));
            j += 2;
        } while (true);
    }
}
