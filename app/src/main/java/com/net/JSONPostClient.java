package com.net;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.WindowManager;

import com.getmorelikes.pro.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class JSONPostClient extends AsyncTask<String, Void, String> {
    AlertDialog progressDialog;
    GetJSONPostListener getJSONListener;
    Context curContext;
    boolean result, blurryBackground;
    String loadingDialog;
    private static List<NameValuePair> mPairs;

    public JSONPostClient(Context context, GetJSONPostListener listener,
                          boolean blurryBackground, String loadingDialog,
                          List<NameValuePair> pairs) {
        this.getJSONListener = listener;
        this.curContext = context;
        this.blurryBackground = blurryBackground;
        this.loadingDialog = loadingDialog;
        mPairs = pairs;
    }

    public JSONPostClient(Activity context, List<NameValuePair> pairs,
                          GetJSONPostListener getJSONPostListener) {
        this.curContext = context;
        this.blurryBackground = blurryBackground;
        this.loadingDialog = loadingDialog;
        this.getJSONListener = getJSONPostListener;

        mPairs = pairs;

    }

    private static String convertStreamToString(InputStream is) {
        /*
		 * To convert the InputStream to String we use the
		 * BufferedReader.readLine() method. We iterate until the BufferedReader
		 * return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is),
                8192);
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public static String connect(String url) {

        HttpParams httpParameters = new BasicHttpParams();
        // Set the timeout in milliseconds until a connection is established.
        // The default value is zero, that means the timeout is not used.
        int timeoutConnection = 3000;
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnection);
        // Set the default socket timeout (SO_TIMEOUT)
        // in milliseconds which is the timeout for waiting for data.
        int timeoutSocket = 5000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        // Prepare a request object
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(mPairs));
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        new M(url);
        // Execute the request
        HttpResponse response = null;
        List<Cookie> cookies = null;
        try {
            response = httpClient.execute(httpPost);

            cookies = httpClient.getCookieStore().getCookies();
            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                for (int i = 0; i < cookies.size(); i++) {
                    Log.d("TAG", "COOKIE " + cookies.get(i).toString());
                    Log.d("TAG", "COOKIE value" + cookies.get(i).getValue());
                }
            }
            // Examine the response status
            Log.i("Praeda", response.getStatusLine().toString());
            // Get hold of the response entity
            HttpEntity entity = response.getEntity();

            if (entity != null) {

                // A Simple JSON Response Read
                InputStream instream = entity.getContent();
                String result = convertStreamToString(instream);
                new M(
                        " =================================================================================================================================");
                new M(
                        " =================================================================================================================================");
                new M(
                        " =================================================================================================================================");
                new M(
                        " ========================================================= RESULT ================================================================");
                new M(" Response " + result);
                new M(
                        " ========================================================== END ==================================================================");
                new M(
                        " =================================================================================================================================");

                // A Simple JSONObject Creation
                JSONObject json = new JSONObject(result);

                // Closing the input stream will trigger connection release
                instream.close();

                return cookies.get(0).getValue();
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception m) {
            m.printStackTrace();
        }

        return cookies.get(0).getValue();
    }

    @Override
    public void onPreExecute() {
    }

    @Override
    protected String doInBackground(String... urls) {
        try {
            return connect(urls[0]);
        } catch (Exception m) {
            m.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String json) {

        getJSONListener.onRemoteCallComplete(json);

    }

    public interface GetJSONPostListener {
        public void onRemoteCallComplete(String json);
    }

    Handler handler = new Handler() {
        @SuppressWarnings("deprecation")
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0) {
                progressDialog = new ProgressDialog(curContext);
                progressDialog.setCancelable(true);
                progressDialog.show();
                progressDialog.setContentView(R.layout.widget_loading_dialog);
                if (blurryBackground) {
                    WindowManager.LayoutParams lp = progressDialog.getWindow()
                            .getAttributes();
                    lp.dimAmount = 0.0f;
                    progressDialog.getWindow().setAttributes(lp);
                    progressDialog.getWindow().addFlags(
                            WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                }
            }
            if (msg.what == 1) {
                if (progressDialog != null) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }
        }
    };
}