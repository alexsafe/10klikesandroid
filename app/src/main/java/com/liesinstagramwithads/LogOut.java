package com.liesinstagramwithads;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import com.net.InstagramSession;

public class LogOut extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LB.getInstagramUser().id = null;
        LB.getInstagramUser().Logout();
        InstagramSession mSession = new InstagramSession(LogOut.this.getActivity().getApplicationContext());
        mSession.resetAccessToken();
        Intent intent = new Intent(LogOut.this.getActivity(), Login.class);
        startActivity(intent);
    }
} 
