package com.liesinstagramwithads;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.adapter.LikesAdapter;
import com.datasets.ItemBatch;
import com.getmorelikes.pro.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.liesinstagramwithads.UrlEngine.GetCompletionListener;
import com.liesinstagramwithads.UrlEngine.GetSaveData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.util.Config.log_tag;

public class SelectPhoto extends DialogFragment {
    ArrayList<ItemBatch> mData = new ArrayList<ItemBatch>();
    ArrayList<ItemBatch> itemes = new ArrayList<ItemBatch>();
    int index = 0;
    TextView coins;
    View view;
    LinearLayout linearLayout;
    GridView grid;

    // private WeakReference<MyAsyncTask> asyncTaskWeakRef;
    //
    // // String campaign_id="0";
    // private void startNewAsyncTask() {
    // MyAsyncTask asyncTask = new MyAsyncTask(SelectPhoto.this.getActivity());
    // this.asyncTaskWeakRef = new WeakReference<MyAsyncTask>(asyncTask);
    // asyncTask.execute();
    // }
    //
    // private boolean isAsyncTaskPendingOrRunning() {
    // return this.asyncTaskWeakRef != null && this.asyncTaskWeakRef.get() !=
    // null && !this.asyncTaskWeakRef.get().getStatus().equals(Status.FINISHED);
    // }
    //
    // private void showEditDialog() {
    // FragmentManager fm = getFragmentManager();
    // EditNameDialog editNameDialog = new EditNameDialog();
    // editNameDialog.show(fm, "fragment_edit_name");
    //
    // }

    void loadNewItems() {
        Log.d("TAG", "loadNewItemsselect");
        UrlEngine.getItemBatchTest(SelectPhoto.this.getActivity(), LB.getInstagramUser().id, new GetCompletionListener() {
            @Override
            public void OnCompletion(ArrayList<ItemBatch> data) {
                if (data == null) {
                    Log.d(log_tag, "loadNewItemsselect completion data null");
                    // Button retry = new Button(getActivity());
                    // retry.setText("Retry");
                    // ((ViewGroup) view).addView(retry);
                    // showEditDialog();
                    AlertDialog ad = new AlertDialog.Builder(getActivity())

                            // Set Dialog Title
                            .setTitle("Server timeout")
                            // Set Dialog Message
                            .setMessage("Click ok to retry")

                            // Positive button
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do something else
                                    Log.d(log_tag, "clicked ok");
                                    loadNewItems();
                                }
                            })

                            // Negative Button
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Log.d(log_tag, "clicked cancel");
                                    // Do something else
                                }
                            }).create();
                    ad.show();
                } else {
                    itemes = new ArrayList<ItemBatch>();
                    itemes.addAll(data);
                    Log.d(log_tag, "loadNewItemsselect in complete: " + itemes.size());
                    // UrlEngine.getStats(SelectPhoto.this.getActivity(),
                    // LB.getInstagramUser().id, itemes.get(index).campaign_id,
                    // "0", new GetSaveData() {
                    //
                    // @Override
                    // public void OnCompletion(JSONObject json) {
                    // // Log.d(log_tag, "on complete:" + json);
                    // // SharedPreferences sharedPrefs =
                    // //
                    // getActivity().getApplicationContext().getSharedPreferences("ig-user",
                    // // 0);
                    // // Editor editor11 = sharedPrefs.edit();
                    // // editor11.putString("likes",
                    // // LB.getInstagramUser().getUserPoints() + "");
                    // // editor11.commit();
                    // registerForContextMenu(view);
                    // if (SelectPhoto.this.getActivity() != null)
                    // SelectPhoto.this.getActivity().invalidateOptionsMenu();
                    // }
                    //
                    // @Override
                    // public void comple(String json) {
                    // // coins.setText(LB.getInstagramUser().getUserPoints() +
                    // // "");
                    // }
                    // }, true);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Log.d(log_tag, "in create select6 photos: "+LB.getInstagramUser());
        // Tracker app_tracker = ((LB)
        // SelectPhoto.this.getActivity().getApplication()).getTracker(LB.TrackerName.APP_TRACKER);
        // Tracker global_tracker = ((LB)
        // this.getActivity().getApplication()).getTracker(LB.TrackerName.GLOBAL_TRACKER);
        // app_tracker.setScreenName("Select Photo");
        // app_tracker.send(new HitBuilders.AppViewBuilder().build());
        // global_tracker.send(new HitBuilders.AppViewBuilder().build());
        // Log.d(log_tag," on create view:"+((LinearLayout)
        // linearLayout).getChildCount());
        // loadNewItems();
        // load();
        setRetainInstance(true);
        // startNewAsyncTask();

        view = inflater.inflate(R.layout.activity_select_photo, container, false);
        grid = (GridView) view.findViewById(R.id.gridLikes);
        // linearLayout = (LinearLayout)
        // view.findViewById(R.id.select_layout_id);


        return view;
    }

    public void getInstagrams() {
        final Activity activity = getActivity();

        UrlEngine.getUserPhotos(activity, new GetSaveData() {
            @Override
            public void OnCompletion(JSONObject json) {
                Log.d("log_tag", "getUserPhotos completion:" + json);
                ArrayList<ItemBatch> mdata = new ArrayList<ItemBatch>();
                ArrayList arraylist;
                JSONArray jsonarray;

                try {
                    if (json != null) {
                        Log.d("TAG", "getUserPhotos json2:" + json);

                        arraylist = new ArrayList();
                        jsonarray = json.getJSONArray("data");
                        // Log.d(log_tag, "getUserPhotos jsonarray:" +
                        // jsonarray);

                        for (int i = 0; i < jsonarray.length(); i++) {
                            ItemBatch data = new ItemBatch();

                            JSONObject mJson = null;
                            try {
                                mJson = jsonarray.getJSONObject(i);
                            } catch (JSONException e1) {

                                e1.printStackTrace();
                            }

                            JSONObject jsonImage = mJson.getJSONObject("images");
                            // Log.d(log_tag, "getUserPhotos jsonImage:" +
                            // jsonImage);
                            jsonImage = jsonImage.getJSONObject("standard_resolution");
                            // Log.d(log_tag, "getUserPhotos jsonImage:" +
                            // jsonImage);
                            String media_url = jsonImage.getString("url");
                            // Log.d(log_tag, "getUserPhotos media_url:" +
                            // media_url);
                            String media_id = mJson.getString("id");

                            JSONObject likesJson = mJson.getJSONObject("likes");
                            String likes = likesJson.getString("count");

                            data.media_id = media_id;
                            data.media_url = media_url;

                            // data.likes = likes;

                            mData.add(data);
                        }

                    } else {

                        activity.finish();
                        // finish();
                        // Intent newIntent = new Intent(Login.this,
                        // Rate.class);
                        LB.getInstagramUser().id = null;
                        LB.getInstagramUser().Logout();
                        Intent newIntent = new Intent(activity, Login.class);
                        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        newIntent.addCategory(Intent.CATEGORY_HOME);
                        activity.startActivity(newIntent);
                        // Log.d(log_tag, "getUserPhotos else mdata:" + mdata);
                        mdata = null;
                    }

                } catch (JSONException e) {
                    // Log.d(log_tag, "getUserPhotos JSONException:" + e);
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // mData = data;
                LikesAdapter adapter = new LikesAdapter(activity, mData);
                grid.setAdapter(adapter);
            }

            @Override
            public void comple(String json) {
                // Log.d("log_tag", "getUserPhotos comple:" + json);
            }
        }, true);

        grid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onGridItemClick((GridView) parent, view, position, id);
            }

        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getInstagrams();

    }

    public void onGridItemClick(GridView g, View v, int position, long id) {
        Activity activity = getActivity();
        if (activity != null) {
            FragmentManager fm = getFragmentManager();

            if (fm != null) {
                FragmentTransaction ft = fm.beginTransaction();
                // Log.d(log_tag, "mdata click :" + mData);
                Pay nextFrag = new Pay();
                Bundle bundle = new Bundle();
                bundle.putString("url", mData.get(position).media_url);
                bundle.putString("id", mData.get(position).media_id);
                bundle.putString("coins", LB.getInstagramUser().getUserPoints() + "");
                ft.addToBackStack(null);
                nextFrag.setArguments(bundle);
                ft.replace(R.id.frame_container_base, nextFrag);
                ft.commit();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Log.d(log_tag, "onstart likes analitics");
        // Get an Analytics tracker to report app starts & uncaught exceptions
        // etc.
        // GoogleAnalytics.getInstance(this).reportActivityStart(this);
        GoogleAnalytics.getInstance(SelectPhoto.this.getActivity().getApplicationContext()).reportActivityStart(this.getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        // Stop the analytics tracking
        GoogleAnalytics.getInstance(SelectPhoto.this.getActivity().getApplicationContext()).reportActivityStop(this.getActivity());
        // GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    private void initUI() {
        // Log.d(log_tag, "iun init ui");
        final GridView grid = (GridView) view.findViewById(R.id.gridLikes);
        // Log.d(log_tag, "ged grid:" + grid);
        UrlEngine.getUserInstagrams(SelectPhoto.this.getActivity(), new GetCompletionListener() {

            @Override
            public void OnCompletion(ArrayList<ItemBatch> data) {
                // Log.d(log_tag, "items batch:" + data);
                mData = data;
                LikesAdapter adapter = new LikesAdapter(SelectPhoto.this.getActivity(), data);
                grid.setAdapter(adapter);
            }
        });

        grid.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent intent = new Intent(SelectPhoto.this.getActivity(), Pay.class);
                intent.putExtra("url", mData.get(position).media_url);
                intent.putExtra("id", mData.get(position).media_id);
                intent.putExtra("coins", LB.getInstagramUser().getUserPoints() + "");
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        // SharedPreferences sharedPrefs =
        // PreferenceManager.getDefaultSharedPreferences(getActivity());
        // Editor editor11 = sharedPrefs.edit();
        // editor11.putString("title","SELECT PHOTO" );
        // editor11.commit();
        getActivity().getActionBar().setTitle(R.string.select_photo);
        registerForContextMenu(view);
        if (SelectPhoto.this.getActivity() != null)
            SelectPhoto.this.getActivity().invalidateOptionsMenu();
        // initUI();

    }

}

class EditNameDialog extends DialogFragment implements OnEditorActionListener {

    private EditText mEditText;

    public EditNameDialog() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment, container);

        getDialog().setTitle("Hello");
        Button b = (Button) view.findViewById(R.id.Retry);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(log_tag, "clicked retry");

            }
        });
        return view;
    }

    public void retry(View v) {
        Log.d(log_tag, "retry clicked");
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        Log.d(log_tag, "onEditorAction");
        return false;
    }
}