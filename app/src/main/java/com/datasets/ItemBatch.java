package com.datasets;

public class ItemBatch {

    public String access_token;
    public String campaign_id;
    public String media_id;
    public String media_url;
    public String user_id;
    public String user_name;
    public String type;
    public String caption;
}
