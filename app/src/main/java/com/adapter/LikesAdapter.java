package com.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.datasets.ItemBatch;
import com.getmorelikes.pro.R;
import com.net.MImageLoader;

import java.util.ArrayList;

public class LikesAdapter extends BaseAdapter {

    private LayoutInflater inflater;

    ArrayList<ItemBatch> data = new ArrayList<ItemBatch>();
    MImageLoader loader;
    Activity activity;
    FragmentManager fragmentManager;

    public LikesAdapter(Activity activity, ArrayList<ItemBatch> data) {
        this.data = data;
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loader = new MImageLoader(activity);

    }

    /**
     * @param convertView The old view to overwrite, if one is passed
     * @returns a ContactEntryView that holds wraps around an ContactEntry
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        TextView tv;
        if (convertView == null) {
            //Log.d(log_tag," convert e null");
            vi = inflater.inflate(R.layout.grid_image, null);
            holder = new ViewHolder();

            holder.profileImage = (ImageView) vi.findViewById(R.id.wallImage);

            // holder.profileListImage.setImageBitmap();

            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        holder.profileImage.setTag(data.get(position).media_url);
        vi.setBackgroundColor(Color.BLACK);
        //Log.d(log_tag,"holder:"+holder);
        //Log.d(log_tag,"profile image:"+holder.profileImage);
        loader.DisplayImage(data.get(position).media_url, activity, holder.profileImage);

        return vi;
    }

    public static class ViewHolder {

        ImageView profileImage;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

}
