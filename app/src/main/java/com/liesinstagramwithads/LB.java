package com.liesinstagramwithads;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.analytics.Tracker;
import com.net.InstagramUser;

import java.util.HashMap;

import static com.util.Config.analitics_id;

public class LB extends Application {

    private static LB instance;
    public static String notificationId;
    public static String profileUrl = "ProfileUrl";
    public static String name = "Name";
    public static String id = "Id";
    public static HashMap<String, String> mPublicKeys = new HashMap<String, String>();

    public static String username = "User";
    public static String accesToken = "Acces";
    public static String followed_by = "Followed_by";
    public static String follows = "Follows";
    public static String media = "Media";
    public static String cookieToken = "";
    private static final String PROPERTY_ID = analitics_id;


    public static int GENERAL_TRACKER = 0;

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:						// roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
        // company.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    static SharedPreferences pref;

    public static String getAccesToken() {
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

        return pref.getString(LB.accesToken, "");

    }

    public static String getProfileUrl() {
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

        return pref.getString(LB.profileUrl, "");
    }

    public static String getName() {
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

        return pref.getString(LB.name, "");
    }

    public static String getId() {
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

        return pref.getString(LB.id, "");
    }

    public static String getUserName() {
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

        return pref.getString(LB.username, "");
    }

    public static String getFollewedBy() {
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

        return pref.getString(LB.followed_by, "");
    }

    public static String getFollows() {
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

        return pref.getString(LB.follows, "");
    }

    public static String getMedia() {
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

        return pref.getString(LB.media, "");
    }

    public static void putString(String type, String string) {
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

        pref.edit().putString(type, string);

        pref.edit().commit();

    }

    public LB() {
        super();
        instance = this;
    }

//	synchronized Tracker getTracker(TrackerName trackerId) {
//		if (!mTrackers.containsKey(trackerId)) {
//			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//			Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics
//					.newTracker(R.xml.app_tracker)
//					: (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics
//							.newTracker(PROPERTY_ID) : analytics
//							.newTracker(R.xml.ecommerce_tracker);
//			mTrackers.put(trackerId, t);
//		}
//		return mTrackers.get(trackerId);
//	}

    public static Context get() {
        return instance;
    }

    // static InstagramUser instagramuser;
    public static InstagramUser getInstagramUser() {
        return InstagramUser.getInstance(instance);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pref = instance.getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);

    }
}
