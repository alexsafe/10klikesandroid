package com.liesinstagramwithads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.datasets.ItemBatch;
import com.getmorelikes.pro.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.liesinstagramwithads.UrlEngine.GetCompletionListener;
import com.net.InstagramApp;
import com.net.InstagramApp.OAuthAuthenticationListener;
import com.net.InstagramSession;
import com.net.InstagramUser;
import com.util.Config;

import java.util.ArrayList;
import java.util.UUID;

import static com.util.Config.log_tag;

public class Login extends Activity {

    GoogleAnalytics mTrackers;
    protected LB GetLikesOnInstagram;
    private InstagramApp mApp;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(log_tag, "create login1");
        super.onCreate(savedInstanceState);


        context = this.getApplicationContext();
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mTrackers = GoogleAnalytics.getInstance(this);

//		((LB) getApplication()).getTracker(LB.TrackerName.APP_TRACKER);
//		GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//		Tracker t = analytics.newTracker(R.xml.global_tracker);

//		t.enableAdvertisingIdCollection(true);
//		t.setScreenName("LoginScreen");
//		t.send(new HitBuilders.AppViewBuilder().build());
        setContentView(R.layout.activity_login_new);
        Log.d(log_tag, "create login2");
        mApp = new InstagramApp(this, Config.CLIENT_ID, Config.CLIENT_SECRET, Config.CALLBACK_URL);
        mApp.setListener(listener);
        UrlEngine.getAppData(Login.this);
        Log.d(log_tag, "create login3");
//		final EditText username = (EditText) findViewById(R.id.username);
//
//		final EditText password = (EditText) findViewById(R.id.password);
        Button login = (Button) findViewById(R.id.login);

//		Log.d(log_tag, "login access_token:" + mApp.hasAccessToken());
//		Log.d(log_tag, "login hasAccessToken:" + mApp.hasAccessToken());
//		Log.d(log_tag, "login getInstagramUser:" + LB.getInstagramUser().id);
        if (LB.getInstagramUser().id != null) {
            Log.d(log_tag, "getInstagramUser");
            if (LB.getInstagramUser().id.length() > 0) {
                finish();
                // Intent newIntent = new Intent(Login.this, Rate.class);
                Intent newIntent = new Intent(Login.this, MainActivity.class);
                newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                newIntent.putExtra("screen", "Rate");
                newIntent.addCategory(Intent.CATEGORY_HOME);
                startActivity(newIntent);
            }
        }
        login.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                Log.d(log_tag, "login on click access_token:" + mApp.hasAccessToken());
                if (mApp.hasAccessToken()) {
                    Log.d(log_tag, "login has :" + mApp.hasAccessToken());
                    final AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                    builder.setMessage("Disconnect from Instagram?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mApp.resetAccessToken();

                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    final AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    Log.d(log_tag, "login mApp auth");
                    mApp.authorize();
                }
            }
        });

        // login.setOnClickListener(new Button.OnClickListener() {
        // @Override
        // public void onClick(View arg0) {
        // // TODO Auto-generated method stub
        // // Log.d(log_tag,"setOnClickListener");
        // UrlEngine.loginUser(Login.this, password.getText().toString(),
        // username.getText().toString(), new TaskComplete() {
        // @Override
        // public void complete(String json, RestNetCaller restNetCaller) {
        //
        // // Log.d(log_tag, "json login error" + json);
        // if (json.equals("error")) {
        // retryDialog(Login.this);
        // } else {
        // UrlEngine.registerUser(Login.this, LB.getInstagramUser().id,
        // username.getText().toString(), new GetCompletionListener() {
        // @Override
        // public void OnCompletion(ArrayList<ItemBatch> data) {
        // finish();
        // // Intent newIntent = new
        // // Intent(Login_.this,
        // // Rate.class);
        // Intent newIntent = new Intent(Login.this, MainActivity.class);
        // newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // newIntent.addCategory(Intent.CATEGORY_HOME);
        // startActivity(newIntent);
        // }
        // });
        // }
        // }
        // });
        // }
        // });
    }

    OAuthAuthenticationListener listener = new OAuthAuthenticationListener() {

        @Override
        public void onSuccess() {
            // tvSummary.setText("Connected as " + mApp.getUserName());
            // btnConnect.setText("Disconnect");
            Log.d(log_tag, "login connected as mother fucking:" + mApp.getUserName());
            Log.d(log_tag, "login connected as mother fucking:" + mApp.getId());

            final InstagramUser instagramuser = LB.getInstagramUser();
            instagramuser.Logout();
            if (instagramuser.uuid == null || instagramuser.uuid.length() < 10) {
                instagramuser.uuid = UUID.randomUUID().toString();
                instagramuser.Save();
            }
            instagramuser.id = mApp.getId();
            // instagramuser.privateInstagrams =
            // jsonobject2.getBoolean("is_private");
            InstagramSession mSession = new InstagramSession(context);
            instagramuser.username = mApp.getUserName();
            instagramuser.access_token = mSession.getAccessToken();
            instagramuser.loggedin = true;
            Log.d(log_tag, "login on success instagramuser:" + instagramuser.username);
            Log.d(log_tag, "login on success instagramuser:" + instagramuser.access_token);
            Log.d(log_tag, "login on success instagramuser:" + instagramuser.loggedin);
            instagramuser.Save();

            UrlEngine.registerUser(Login.this, mApp.getId(), mApp.getUserName().toString(), new GetCompletionListener() {
                @Override
                public void OnCompletion(ArrayList<ItemBatch> data) {
                    finish();
                    // Intent newIntent = new Intent(Login_auth.this,
                    // Rate.class);

                    Intent newIntent = new Intent(Login.this, MainActivity.class);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    newIntent.addCategory(Intent.CATEGORY_HOME);
                    newIntent.putExtra("screen", "Rate");
                    startActivity(newIntent);
                }
            });
        }

        @Override
        public void onFail(String error) {
            // Toast.makeText(MainActivity.this, error,
            // Toast.LENGTH_SHORT).show();
            Log.d(log_tag, "login connected as fail:" + error);
        }
    };

    public static void retryDialog(final Activity act) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);

        builder.setTitle("Oops");
        builder.setMessage("Your account or password does not match with your Instagram account");

        builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
            }

        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
