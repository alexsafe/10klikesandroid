package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.datasets.ItemBatch;
import com.getmorelikes.pro.R;
import com.net.MImageLoader;

import java.util.ArrayList;

import static com.util.Config.log_tag;


public class ViewPagerAdapter extends PagerAdapter {
    // Declare Variables
    Context context;
    int[] flag;
    LayoutInflater inflater;
    ArrayList<ItemBatch> items;
    MImageLoader loader;
    Activity activity;


    public ViewPagerAdapter(Context context, int[] flag) {
        this.context = context;
        this.flag = flag;

    }

    public ViewPagerAdapter(Context context, ArrayList<ItemBatch> items, Activity activity) {
        this.context = context;
        this.items = items;
        this.activity = activity;

    }

    //	@Override
//	public int getCount() {
//		return flag.length;
//	}
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // Declare Variables

        ImageView imgflag;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item, container, false);

        // Locate the TextViews in viewpager_item.xml
//		txtrank = (TextView) itemView.findViewById(R.id.rank);
//		txtcountry = (TextView) itemView.findViewById(R.id.country);
//		txtpopulation = (TextView) itemView.findViewById(R.id.population);
//
//		// Capture position and set to the TextViews
//		txtrank.setText(rank[position]);
//		txtcountry.setText(country[position]);
//		txtpopulation.setText(population[position]);

        // Locate the ImageView in viewpager_item.xml
        imgflag = (ImageView) itemView.findViewById(R.id.flag);
        // Capture position and set to the ImageView
        //imgflag.setImageResource(flag[position]);
//		Log.d(log_tag,"instantiateItem items:"+items);
//		Log.d(log_tag,"instantiateItem items.get(position).media_url:"+items.get(position).media_url);
//		Log.d(log_tag,"instantiateItem position:"+position);
//		Log.d(log_tag,"instantiateItem activity:"+activity);
//		Log.d(log_tag,"instantiateItem imgflag:"+imgflag);
        Log.d(log_tag, "instantiateItem loader1:" + loader);
        loader = new MImageLoader(activity.getApplicationContext());
        Log.d(log_tag, "instantiateItem loader2:" + loader);
        loader.DisplayImage(items.get(position).media_url, activity, imgflag);
        imgflag.setTag(items.get(position).media_url);
        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
