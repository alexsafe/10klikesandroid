package com.liesinstagramwithads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import com.datasets.ItemBatch;
import com.datasets.RegisterData;
import com.net.GCMRegistrar;
import com.net.InstagramSession;
import com.net.InstagramUser;
import com.net.JSONClient;
import com.net.JSONClient.GetJSONListener;
import com.net.JSONClient2;
import com.net.JSONClientImage;
import com.net.JSONPostClient;
import com.net.JSONPostClient.GetJSONPostListener;
import com.net.M;
import com.net.PersistentCookieStore;
import com.net.RestNetCaller;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import static com.util.Config.baseUrl;
import static com.util.Config.log_tag;

public class UrlEngine {

    private static final int UTF = 0;
    // static String baseUrl = "http://soma.greenlab.ro/10000likes/";
    static String serverCall = baseUrl + "/%method.php?";

    public static boolean getAppData(final Activity activity) {
        StringBuffer link = new StringBuffer();
        String returnStr = "";
        link.append(baseUrl + "app_data.php?");
        Log.d(log_tag, "getappdata json:" + link);
        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                Log.d(log_tag, "getappdata json:" + success);
                if (success) {
                    try {
                        Log.d(log_tag, "getappdata json:" + json);
                        if (json != null) {

                            String access_token = json.getString("access_token");
                            String NActions = json.getString("NActions");
                            String PModal = json.getString("PModal");
                            String PShare = json.getString("PShare");
                            String URLtoShare = json.getString("URLtoShare");
                            String adMobId = json.getString("adMobId");
                            String ratePackage = json.getString("ratePackage");
                            String proVersion = json.getString("proVersion");
                            String PRate = json.getString("PRate");
                            String timeout = json.getString("timeout");
                            // returnStr+=access_token+","+NActions+","+PModal+","+PShare+","+URLtoShare;
                            android.content.SharedPreferences.Editor editor = activity.getApplicationContext().getSharedPreferences("ig-user", 0).edit();
                            editor.putString("access_token", access_token);
                            editor.putInt("noActions", 11);
                            editor.putString("NActions", NActions);
                            editor.putString("PModal", PModal);
                            editor.putString("PShare", PShare);
                            editor.putString("URLtoShare", URLtoShare);
                            editor.putString("ratePackage", ratePackage);
                            editor.putString("adMobId", adMobId);
                            editor.putString("proVersion", proVersion);
                            editor.putString("PRate", PRate);
                            editor.putString("timeout", timeout);
                            editor.commit();
                            Log.d(log_tag, "urlengine timeout: " + timeout);
                        }
                        // LB.mData = mData;
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
            }

        }, false, "Getting app data...", false, false);
        client.execute(link.toString());
        return true;
    }

    public static boolean registerUser(final Activity activity, String instaId, String userName, final GetCompletionListener listener) {
        Log.d(log_tag, "registerUser : ");
        Log.d(log_tag, "registerUser userName: " + userName);
        StringBuffer link = new StringBuffer();
        link.append(baseUrl + "/register_user.php?");

        link.append("client=");
        link.append("2");

        link.append("&insta_id=");
        link.append(instaId);

        link.append("&insta_user=");
        link.append(userName);

        String token = LB.getInstagramUser().uuid;
        link.append("&token=");
        link.append(token);

        String access_token = LB.getInstagramUser().access_token;
        link.append("&access_token=");
        link.append(access_token);

        link.append("&security=");
        link.append(M.MD5(instaId + token));

        link.append("&device=");
        link.append(M.getDeviceId(activity));
        Log.d(log_tag, "registerUser link: " + link);
        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                Log.d(log_tag, "registerUser json: " + json);
                Log.d(log_tag, "registerUser success: " + success);
                final List<RegisterData> mData = new ArrayList<RegisterData>();
                if (success) {
                    try {
                        if (json != null) {
                            Log.d(log_tag, "login json:" + json);
                            String operation = json.getString("operation");
                            String coins = json.getString("coins");
                            // String multiplier = json.getString("multiplier");
                            // String premium = json.getString("premium");
                            // String max_promos = json.getString("max_promos");
                            JSONArray array = null;
                            try {
                                array = json.getJSONArray("items");
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject mJson = null;
                                try {
                                    mJson = array.getJSONObject(i);
                                } catch (JSONException e1) {

                                    e1.printStackTrace();
                                }
                                SharedPreferences sharedpreferences = activity.getSharedPreferences("ig-user", 0);
                                android.content.SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putInt("rated", 0);
                                editor.commit();
                                RegisterData data = new RegisterData();
                                data.campaign_id = mJson.getInt("campaign_id");
                                data.media_id = mJson.getString("media_id");
                                data.media_url = mJson.getString("media_url");
                                data.likes = mJson.getString("likes");
                                data.skips = mJson.getString("skips");
                                data.target = mJson.getString("target");
                                LB.getInstagramUser().points = json.getInt("coins");
                                LB.getInstagramUser().syncUserPoints();
                                mData.add(data);

                            }
                        }
                        // LB.mData = mData;
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
                if (listener != null) {
                    listener.OnCompletion(null);
                }
            }

        }, true, "Registering...", false, true);
        client.execute(link.toString());
        return true;

    }

    public static int getNoOfLikes(final Activity activity, String mediaId, String instaId, final GetData listener) {
        StringBuffer link = new StringBuffer();
        String clientId = "3189111e541c4a90bdd2ae63bf4b50b9";
        String clientSecret = "75da2b49ff504a98ad96a06d657358cb";
        String callbackUrl = "http://soma.greenlab.ro/10000likes/register_user.php";
        SharedPreferences sharedpreferences = activity.getApplicationContext().getSharedPreferences("ig-user", 0);
        String accessToken = sharedpreferences.getString("access_token", null);
        // Log.d(log_tag, "getNoOfLikes token:" + accessToken);
        // accessToken="1496142301.4ea7844.c94a7715d0e74cc98d0ba145cc9a145d";
        int size = 0;

        // instap.refreshToken();
        // instap.getAccountList();
        String urlString = "https://api.instagram.com/v1/media/" + mediaId + "?access_token=" + accessToken;
        // //Log.d(log_tag, "getNoOfLikes mediaId:" + mediaId);
        // //Log.d(log_tag, "getNoOfLikes urlString:" + urlString);

        // link.append("https://api.instagram.com/v1/media/846984972100434942_1496142301/likes?access_token=1496142301.5b9e1e6.30b5cef065024208881941599a06b9ea");
        // link.append("https://api.instagram.com/v1/media/846984972100434942_1496142301/likes?access_token=1496142301.467ede5.a686135da52f40f18631c9163f1610c3");
        // link.append("https://api.instagram.com/v1/media/846984972100434942_1496142301/likes?access_token=1496142301.4ea7844.c94a7715d0e74cc98d0ba145cc9a145d");
        // link.append("https://api.instagram.com/v1/media/846984972100434942_1496142301?access_token=1496142301.4ea7844.c94a7715d0e74cc98d0ba145cc9a145d");
        link.append(urlString);
        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                if (success) {
                    // //Log.d(log_tag, "likess json: " + json);

                    JSONArray array = null;

                    try {
                        if (json != null) {
                            //
                            JSONObject data = json.getJSONObject("data");
                            JSONObject likesJson = data.getJSONObject("likes");
                            listener.OnCompletion(likesJson);
                            // //Log.d(log_tag, "likess data: " + data);
                            // //Log.d(log_tag, "likess likesJson: " +
                            // likesJson);
                            // //Log.d(log_tag, "likess likesJson likes: " +
                            // likesJson.getString("count"));
                        }
                    } catch (JSONException e1) {
                        // Log.d(log_tag, "JSONException: " + e1);
                        e1.printStackTrace();
                    }

                    //

                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
                // if (listener != null) {
                // listener.OnCompletion(mdata);
                // }
            }

        }, true, "Getting number of likes...", false, false);
        client.execute(link.toString());
        return size;
    }

    public static boolean starCampaign(final Activity activity, String mediaId, String mediaUrl, String target, int coins, String instaId, final GetSaveData listener) {

        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "campaign"));
        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {

            e1.printStackTrace();
        }

        link.append("&media_id=");
        link.append(mediaId);

        link.append("&media_url=");
        link.append(mediaUrl);

        link.append("&target=");
        link.append(target);

        link.append("&coins=");
        link.append(coins);

        link.append("&insta_id=");
        link.append(instaId);

        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {

                if (success) {
                    try {
                        if (json != null) {
                            Log.d(log_tag, "starCampaign:" + json);
                            LB.getInstagramUser().points = json.getInt("coins");
                            LB.getInstagramUser().syncUserPoints();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
                if (listener != null) {
                    listener.OnCompletion(json);
                }
            }

        }, false, "Starting the new campaign.", true, false);
        client.execute(link.toString());
        return true;

    }

    public static boolean getItemsBatch(final Activity activity, final String instaId, final GetCompletionListener listener) {

        // Log.d("TAG", "enter");
        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "get_items"));

        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {

            e1.printStackTrace();
        }

        link.append("&insta_id=");
        // instaId = LB.id;
        link.append(instaId);
        Log.d(log_tag, "getItemsBatch: " + link);
        JSONClient2 client = new JSONClient2(activity, new JSONClient2.GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                Log.d(log_tag, "getItemsBatch json: " + json);
                final ArrayList<ItemBatch> mdata = new ArrayList<ItemBatch>();
                final ArrayList<ItemBatch> mdataFail = null;

                if (success) {
                    try {
                        if (json != null) {
                            JSONArray array = null;
                            try {

                                array = json.getJSONArray("items");
                            } catch (JSONException e1) {

                                e1.printStackTrace();
                            }

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject mJson = null;
                                try {
                                    mJson = array.getJSONObject(i);
                                } catch (JSONException e1) {

                                    e1.printStackTrace();
                                }
                                // Log.d(log_tag,
                                // "getItemsBatchjson batch campaign: " +
                                // mJson.getString("campaign_id"));
                                // Log.d(log_tag,
                                // "getItemsBatchjson batch media_id: " +
                                // mJson.getString("media_id"));
                                // Log.d(log_tag,
                                // "getItemsBatchjson batch media_url: " +
                                // mJson.getString("media_url"));
                                final ItemBatch data = new ItemBatch();
                                data.campaign_id = mJson.getString("campaign_id");
                                data.media_id = mJson.getString("media_id");
                                data.media_url = mJson.getString("media_url");

                                data.user_id = mJson.getString("user_id");
                                data.user_name = mJson.getString("user_name");
                                data.type = mJson.getString("type");
                                data.caption = mJson.getString("caption");
                                LB.getInstagramUser().points = json.getInt("coins");
                                LB.getInstagramUser().syncUserPoints();
                                checkBrokenImage(activity, data.media_url, new GetCompletionListener() {
                                    @Override
                                    public void OnCompletion(ArrayList<ItemBatch> imageData) {
                                        Log.d(log_tag, "checkBrokenImage completion imagedata:" + imageData.size());
                                        Log.d(log_tag, "checkBrokenImage completion imagedata:" + imageData.get(0).media_id);
                                        if (imageData.get(0).media_id.equals("found")) {
                                            mdata.add(data);
                                        }
                                    }
                                });

                            }
                            // Log.d(log_tag, "mdata size: " + mdata.size());
                            if (mdata.size() < 1) {

                                UrlEngine.getPopularImages(activity, new GetCompletionListener() {
                                    @Override
                                    public void OnCompletion(ArrayList<ItemBatch> newData) {
                                        ArrayList<ItemBatch> shuffledItems = new ArrayList<ItemBatch>();
                                        for (ItemBatch c : mdata) {
                                            shuffledItems.add(c);
                                        }
                                        for (ItemBatch p : newData) {
                                            shuffledItems.add(p);
                                            if (shuffledItems.size() == 1)
                                                break;
                                        }
                                        Collections.shuffle(shuffledItems);
                                        // Log.d("TAG", "itemes size is sssss" +
                                        // shuffledItems.size());
                                        listener.OnCompletion(shuffledItems);
                                    }
                                });
                            } else {
                                listener.OnCompletion(mdata);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!success) {
                    Log.d(log_tag, "getItemsBatch not success");
                    // M.doErrorCheck(activity, json);
                    listener.OnCompletion(mdataFail);
                }
                if (listener != null) {
                    // listener.OnCompletion(mdata);
                }
            }

        }, false, "Getting fresh photo...", false, false);
        client.execute(link.toString());
        return true;
    }

    public static boolean getItemDetails(final Activity activity, final String instaId, final GetDataToken getDataToken) {
        // Log.d(log_tag, "getItemDetails");
        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "get_item"));

        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {

            e1.printStackTrace();
        }

        link.append("&insta_id=");
        // instaId = LB.id;
        link.append(instaId);
        // Log.d(log_tag, "getItemDetails link append:" + link);

        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {

                final ArrayList<ItemBatch> mdata = new ArrayList<ItemBatch>();
                String access_token = "0";

                if (success) {
                    JSONArray array = null;
                    try {
                        if (json != null) {
                            access_token = json.getString("token");
                            // Log.d(log_tag, "getItemDetails token in: " +
                            // access_token);
                            array = json.getJSONArray("items");
                        }
                    } catch (JSONException e1) {

                        e1.printStackTrace();
                    }

                    // Log.d(log_tag, "getItemDetails access_token: " +
                    // access_token);

                    getDataToken.OnCompletion(access_token);
                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
                if (getDataToken != null) {
                    // listener.OnCompletion(mdata);
                }
            }

        }, false, "Getting fresh photo...", false, false);
        client.execute(link.toString());
        return true;
    }

    private static String streamToString(InputStream is) throws IOException {
        String str = "";

        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                reader.close();
            } finally {
                is.close();
            }

            str = sb.toString();
        }

        return str;
    }

    public static boolean likeWithInsta(final Activity activity, String media_id, final GetSaveData listener, boolean showHart) {
        Log.d(log_tag, "urlengine likeWithInsta:");

        InstagramUser instagramuser = LB.getInstagramUser();
        InstagramSession mSession = new InstagramSession(activity.getApplicationContext());
        Log.d(log_tag, "likeWithInsta access_token:" + instagramuser.access_token);
        Log.d(log_tag, "likeWithInsta access_token mSession:" + mSession.getAccessToken());
        String link = "https://api.instagram.com/v1/users/self/feed?access_token=" + mSession.getAccessToken();

        URL url;
        try {
            url = new URL("https://api.instagram.com/v1/media/");

            // String link =
            // "https://api.instagram.com/v1/users/self/feed?access_token=1496142301.1fb234f.0b9e200e57ce4647ad2bc6e963808d02";

            Log.d(log_tag, "getUserPhotos access_token:" + instagramuser.access_token);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            // urlConnection.connect();
            OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
            writer.write(media_id + "/likes?access_token=" + instagramuser.access_token);
            writer.flush();
            String response = streamToString(urlConnection.getInputStream());
            Log.d(log_tag, "likeWithInsta response:" + response);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            Log.d(log_tag, "likeWithInsta MalformedURLException:" + e);
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.d(log_tag, "likeWithInsta IOException:" + e);
            e.printStackTrace();
        }
        /*
		 * JSONClient2 client = new JSONClient2(activity, new
		 * JSONClient2.GetJSONListener() {
		 * 
		 * @Override public void onRemoteCallComplete(JSONObject json, boolean
		 * success) { Log.d(log_tag, "likeWithInsta json:" + json); if (success)
		 * { // try { if (json != null) { Log.d(log_tag, "likeWithInsta json:" +
		 * json);
		 * 
		 * LB.getInstagramUser().syncUserPoints(); // String max_promos =
		 * json.getString("max_promos"); } // } catch (JSONException e) { //
		 * Log.d(log_tag, "json JSONException:" + e); // e.printStackTrace(); //
		 * } } else if (!success) { M.doErrorCheck(activity, json); } if
		 * (listener != null) { listener.OnCompletion(json); } } }, false,
		 * "Fetching a new fucking photo 2...", false, false);
		 * client.execute(link.toString());
		 */
        return true;

    }

    public static boolean getUserPhotos(final Activity activity, final GetSaveData listener, boolean showHart) {
        Log.d(log_tag, "urlengine getUserPhotos:");

        InstagramUser instagramuser = LB.getInstagramUser();
        InstagramSession mSession = new InstagramSession(activity.getApplicationContext());
        Log.d(log_tag, "getUserPhotos access_token:" + instagramuser.access_token);
        Log.d(log_tag, "getUserPhotos access_token mSession:" + mSession.getAccessToken());
        // String link =
        // "https://api.instagram.com/v1/users/self/feed?access_token=1496142301.1fb234f.0b9e200e57ce4647ad2bc6e963808d02";
        //String link = "https://api.instagram.com/v1/users/self/feed?access_token=" + mSession.getAccessToken();
        String link = "https://api.instagram.com/v1/users/" + mSession.getId() + "/media/recent/?access_token=" + mSession.getAccessToken();
        Log.d(log_tag, "getUserPhotos access link:" + link);
        JSONClient2 client = new JSONClient2(activity, new JSONClient2.GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                Log.d(log_tag, "getUserPhotos json:" + json);
                if (success) {
                    // try {
                    if (json != null) {
                        Log.d(log_tag, "getUserPhotos json getUserPhotos:" + json);

                        LB.getInstagramUser().syncUserPoints();
                        // String max_promos = json.getString("max_promos");
                    }
                    // } catch (JSONException e) {
                    // Log.d(log_tag, "json JSONException:" + e);
                    // e.printStackTrace();
                    // }
                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
                if (listener != null) {
                    listener.OnCompletion(json);
                }
            }
        }, false, "Fetching a new fucking photo 2...", true, false);
        client.execute(link.toString());
        return true;

    }

    public static boolean getItemBatch(final Activity activity, final String instaId, final GetCompletionListener listener) {

        Log.d("TAG", "getItemBatch enter");
        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "get_item"));

        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {

            e1.printStackTrace();
        }

        link.append("&insta_id=");
        // instaId = LB.id;
        link.append(instaId);
        Log.d(log_tag, "getItemBatch link: " + link);
        // MyAsyncTask asyncTask = new MyAsyncTask(activity);
        // this.asyncTaskWeakRef = new WeakReference<MyAsyncTask>(asyncTask);
        // asyncTask.execute();
        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                Log.d(log_tag, "getItemBatch complete: ");
                Log.d(log_tag, "getItemBatch json: " + json);
                final ArrayList<ItemBatch> mdata = new ArrayList<ItemBatch>();
                final ArrayList<ItemBatch> mdataFail = null;

                if (success) {
                    try {
                        if (json != null) {
                            JSONArray array = null;
                            try {

                                array = json.getJSONArray("items");
                            } catch (JSONException e1) {

                                e1.printStackTrace();
                            }

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject mJson = null;
                                try {
                                    mJson = array.getJSONObject(i);
                                } catch (JSONException e1) {

                                    e1.printStackTrace();
                                }
                                // //Log.d(log_tag, "json batch: " + mJson);
                                ItemBatch data = new ItemBatch();
                                data.campaign_id = mJson.getString("campaign_id");
                                data.media_id = mJson.getString("media_id");
                                data.media_url = mJson.getString("media_url");

                                data.user_id = mJson.getString("user_id");
                                data.user_name = mJson.getString("user_name");
                                data.type = mJson.getString("type");
                                data.caption = mJson.getString("caption");
                                LB.getInstagramUser().points = json.getInt("coins");
                                Log.d(log_tag, "getItemBatch coins:" + json.getInt("coins"));
                                LB.getInstagramUser().syncUserPoints();
                                mdata.add(data);
                                // registerForContextMenu(view);
                                if (activity != null)
                                    activity.invalidateOptionsMenu();
                            }
                            // Log.d(log_tag, "mdata size: " + mdata.size());
                            if (mdata.size() < 1) {

                                UrlEngine.getPopularImages(activity, new GetCompletionListener() {
                                    @Override
                                    public void OnCompletion(ArrayList<ItemBatch> newData) {
                                        ArrayList<ItemBatch> shuffledItems = new ArrayList<ItemBatch>();
                                        for (ItemBatch c : mdata) {
                                            shuffledItems.add(c);
                                        }
                                        for (ItemBatch p : newData) {
                                            shuffledItems.add(p);
                                            if (shuffledItems.size() == 1)
                                                break;
                                        }
                                        Collections.shuffle(shuffledItems);
                                        Log.d("TAG", "itemes size is sssss" + shuffledItems.size());
                                        listener.OnCompletion(shuffledItems);
                                    }
                                });
                            } else {
                                listener.OnCompletion(mdata);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!success) {
                    Log.d(log_tag, "getItemBatch not success");
                    listener.OnCompletion(mdataFail);
                    // M.doErrorCheck(activity, json);
                }
                if (listener != null) {
                    // listener.OnCompletion(mdata);
                }
            }

        }, false, "Loading ...", false, false);
        client.execute(link.toString());
        return true;
    }

    public static boolean getItemBatchTest(final Activity activity, final String instaId, final GetCompletionListener listener) {

        Log.d("TAG", "getItemBatchTest enter");
        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "get_item"));

        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {

            e1.printStackTrace();
        }

        link.append("&insta_id=");
        // instaId = LB.id;
        link.append(instaId);
        Log.d(log_tag, "getItemBatchTest: " + link);
        // MyAsyncTask asyncTask = new MyAsyncTask(activity);
        // this.asyncTaskWeakRef = new WeakReference<MyAsyncTask>(asyncTask);
        // asyncTask.execute();
        JSONClient2 client = new JSONClient2(activity, new JSONClient2.GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                Log.d(log_tag, "getItemBatchTest complete: ");
                Log.d(log_tag, "getItemBatchTest json: " + json);
                Log.d(log_tag, "getItemBatchTest success: " + success);

                final ArrayList<ItemBatch> mdata = new ArrayList<ItemBatch>();
                final ArrayList<ItemBatch> mdataFail = null;

                if (success) {
                    try {
                        if (json != null) {
                            JSONArray array = null;
                            try {

                                array = json.getJSONArray("items");
                            } catch (JSONException e1) {

                                e1.printStackTrace();
                            }

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject mJson = null;
                                try {
                                    mJson = array.getJSONObject(i);
                                } catch (JSONException e1) {

                                    e1.printStackTrace();
                                }
                                // //Log.d(log_tag, "json batch: " + mJson);
                                ItemBatch data = new ItemBatch();
                                data.campaign_id = mJson.getString("campaign_id");
                                data.media_id = mJson.getString("media_id");
                                data.media_url = mJson.getString("media_url");

                                data.user_id = mJson.getString("user_id");
                                data.user_name = mJson.getString("user_name");
                                data.type = mJson.getString("type");
                                data.caption = mJson.getString("caption");
                                LB.getInstagramUser().points = json.getInt("coins");
                                LB.getInstagramUser().syncUserPoints();
                                mdata.add(data);
                            }
                            // Log.d(log_tag, "mdata size: " + mdata.size());
                            if (mdata.size() < 1) {

                                UrlEngine.getPopularImages(activity, new GetCompletionListener() {
                                    @Override
                                    public void OnCompletion(ArrayList<ItemBatch> newData) {
                                        ArrayList<ItemBatch> shuffledItems = new ArrayList<ItemBatch>();
                                        for (ItemBatch c : mdata) {
                                            shuffledItems.add(c);
                                        }
                                        for (ItemBatch p : newData) {
                                            shuffledItems.add(p);
                                            if (shuffledItems.size() == 1)
                                                break;
                                        }
                                        Collections.shuffle(shuffledItems);
                                        Log.d("TAG", "itemes size is sssss" + shuffledItems.size());
                                        listener.OnCompletion(shuffledItems);
                                    }
                                });
                            } else {
                                listener.OnCompletion(mdata);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!success) {
                    Log.d(log_tag, "getItemBatchTest not success");
                    listener.OnCompletion(mdataFail);
                    M.doErrorCheck(activity, json);
                }
                if (listener != null) {
                    // listener.OnCompletion(mdata);
                }
            }

        }, false, "Loading items...", false, false);
        client.execute(link.toString());
        // try {
        // client.execute(link.toString());//.get(1000, TimeUnit.MILLISECONDS);
        // } catch (InterruptedException e) {
        // Log.d(log_tag, "getItemBatchTest InterruptedException:" + e);
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // } catch (ExecutionException e) {
        // // TODO Auto-generated catch block
        // Log.d(log_tag, "getItemBatchTest ExecutionException:" + e);
        // e.printStackTrace();
        // } catch (TimeoutException e) {
        // // TODO Auto-generated catch block
        // Log.d(log_tag, "getItemBatchTest TimeoutException:" + e);
        // e.printStackTrace();
        // client.cancel(true);
        // }
        // ;
        return true;
    }

    public static boolean checkBrokenImage(final Activity activity, String image, final GetCompletionListener listener) {

        StringBuffer link = new StringBuffer();

        // String image_url =
        // "http://photos-e.ak.instagram.com/hphotos-ak-xaf1/t51.2885-15/e15/10986120_1405355496433788_1538731634_n.jpg";
        // String image_url =
        // "http://photos-g.ak.instagram.com/hphotos-ak-xaf1/t51.2885-15/e15/10914459_424736601012054_253468816_n.jpg";
        // link.append("https://api.instagram.com/v1/media/popular?client_id=1f7f1182dea340eb82da2bc21261e780&count=5");
        link.append(image);
        Log.d(log_tag, "checkBrokenImage link:" + link);
        JSONClientImage client = new JSONClientImage(activity, new JSONClientImage.GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {

                ArrayList<ItemBatch> mData = new ArrayList<ItemBatch>();
                Log.d(log_tag, "checkBrokenImage json:" + json + " success:" + success);
                if (success) {
                    try {
                        if (json != null) {
                            JSONArray array = null;
                            if (json.has("content")) {
                                String broken = json.getString("content");
                                ItemBatch data = new ItemBatch();

                                data.media_id = broken;
                                mData.add(data);
                            }
                        }
                        // LB.mData = mData;
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    // listener.OnCompletion(mData,true);
                    listener.OnCompletion(mData);
                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }

            }

        }, false, "Getting fresh photos...", false, false);
        client.execute(link.toString());
        return true;

    }

    public static boolean getPopularImages(final Activity activity, final GetCompletionListener listener) {

        StringBuffer link = new StringBuffer();
        link.append("https://api.instagram.com/v1/media/popular?client_id=1f7f1182dea340eb82da2bc21261e780&count=5");

        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {

                ArrayList<ItemBatch> mData = new ArrayList<ItemBatch>();

                if (success) {
                    try {
                        if (json != null) {
                            JSONArray array = null;
                            try {
                                array = json.getJSONArray("data");
                            } catch (JSONException e1) {

                                e1.printStackTrace();
                            }

                            for (int i = 0; i < array.length(); i++) {
                                ItemBatch data = new ItemBatch();

                                JSONObject mJson = null;
                                try {
                                    mJson = array.getJSONObject(i);
                                } catch (JSONException e1) {

                                    e1.printStackTrace();
                                }
                                JSONObject jsonImage = mJson.getJSONObject("images");
                                jsonImage = jsonImage.getJSONObject("standard_resolution");
                                String media_url = jsonImage.getString("url");
                                String media_id = mJson.getString("id");

                                JSONObject likesJson = mJson.getJSONObject("likes");
                                String likes = likesJson.getString("count");

                                data.media_id = media_id;
                                data.media_url = media_url;

                                // data.likes = likes;

                                mData.add(data);

                            }
                        }
                        // LB.mData = mData;
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    // listener.OnCompletion(mData,true);
                    listener.OnCompletion(mData);
                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }

            }

        }, false, "Getting fresh photos...", false, false);
        client.execute(link.toString());
        return true;

    }

    public static boolean getInstagramPhotos(final Activity activity, String instaId, final GetCompletionListener listener) {

        StringBuffer link = new StringBuffer();
        link.append("https://api.instagram.com/v1/users/self/media/recent/?client_id=" + LB.getInstagramUser().id);

        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {

                ArrayList<ItemBatch> mdata = new ArrayList<ItemBatch>();
                if (success) {
                    if (json != null) {
                        JSONArray array = null;
                        try {
                            array = json.getJSONArray("data");
                        } catch (JSONException e1) {

                            e1.printStackTrace();
                        }

                        for (int i = 0; i < array.length(); i++) {
                            ItemBatch item = new ItemBatch();
                            JSONObject mJson = null;
                            try {
                                mJson = array.getJSONObject(i);
                            } catch (JSONException e1) {

                                e1.printStackTrace();
                            }

                            try {
                                mJson.getString("id");

                                JSONObject standard = mJson.getJSONObject("images");
                                standard = standard.getJSONObject("standard_resolution");
                                String url = standard.getString("url");

                                String id = mJson.getString("id");

                                Log.d("String TAG", "the tag is " + url);
                                Log.d("String TAG", "the tag is " + id);

                                item.media_url = standard.getString("url");
                                item.media_id = id;
                                mdata.add(item);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        //
                    }

                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
                if (listener != null) {
                    listener.OnCompletion(mdata);
                }
            }

        }, true, "Getting bucket photos...", false, true);
        client.execute(link.toString());
        return true;

    }

    public static boolean addUltraFastLikes(final Activity activity, String instaId, String campaign_id, String target, final GetCompletionListener getCompletionListener) {

        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "item_autolikes"));

        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        }
        link.append("client=");
        link.append(pInfo.versionName);

        link.append("&insta_id=");
        link.append(instaId);

        link.append("&campaign_id=");
        link.append(campaign_id);

        link.append("&likes=");
        link.append(target);

        link.append("&response=");
        link.append("1");

        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                if (success) {
                    try {
                        if (json != null) {
                            String operation = json.getString("operation");
                            String coins = json.getString("coins");
                            String multiplier = json.getString("multiplier");
                            String premium = json.getString("premium");
                            LB.getInstagramUser().points = json.getInt("coins");
                            LB.getInstagramUser().syncUserPoints();

                            LB.getInstagramUser().syncUserPoints();
                            String max_promos = json.getString("max_promos");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
                if (getCompletionListener != null) {
                    getCompletionListener.OnCompletion(null);
                }
            }
        }, true, "Fetching a new photo ...", false, false);
        client.execute(link.toString());
        return true;

    }

    public static boolean getStats(final Activity activity, String instaId, String campaign_id, String response, final GetSaveData listener, boolean showHart) {
        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "item_stats"));

        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        }
        link.append("client=");
        link.append(pInfo.versionName);

        link.append("&insta_id=");
        link.append(instaId);

        link.append("&campaign_id=");
        link.append(campaign_id);

        link.append("&response=");
        link.append(response);
        Log.d(log_tag, "link: " + link);
        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                if (success) {
                    try {
                        if (json != null) {
                            Log.d(log_tag, "json success getStats :" + json);
                            String operation = json.getString("operation");
                            String coins = json.getString("coins");
                            String multiplier = json.getString("multiplier");
                            String premium = json.getString("premium");
                            Log.d(log_tag, "urlengine:" + coins);
                            Log.d(log_tag, "LB.getInstagramUser().points:" + LB.getInstagramUser().points);
                            if (coins != null)
                                if (!coins.equals(null))
                                    if (!coins.equals("null"))
                                        LB.getInstagramUser().points = Integer.parseInt(coins);
                            LB.getInstagramUser().syncUserPoints();
                            LB.getInstagramUser().syncUserPoints();
                            String max_promos = json.getString("max_promos");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
                if (listener != null) {
                    listener.OnCompletion(json);
                }
            }
        }, true, "Getting data...", false, false);
        client.execute(link.toString());
        return true;
    }

    public static boolean respondItem(final Activity activity, String instaId, String campaign_id, String response, final GetSaveData listener, boolean showHart) {
        Log.d(log_tag, "urlengine respondItem campaign_id:" + campaign_id);
        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "item"));

        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        }
        link.append("client=");
        link.append(pInfo.versionName);

        link.append("&insta_id=");
        link.append(instaId);

        link.append("&campaign_id=");
        link.append(campaign_id);

        link.append("&response=");
        link.append(response);

        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                if (success) {
                    try {
                        if (json != null) {
                            // Log.d(log_tag, "json respondItem:" + json);
                            String operation = json.getString("operation");
                            String coins = json.getString("coins");
                            String multiplier = json.getString("multiplier");
                            String premium = json.getString("premium");
                            LB.getInstagramUser().points = json.getInt("coins");
                            LB.getInstagramUser().syncUserPoints();

                            LB.getInstagramUser().syncUserPoints();
                            String max_promos = json.getString("max_promos");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!success) {
                    M.doErrorCheck(activity, json);
                }
                if (listener != null) {
                    listener.OnCompletion(json);
                }
            }
        }, true, "Fetching a new photo 2...", false, false);
        client.execute(link.toString());
        return true;

    }

    public static void getUserInstagrams(final Activity activity, final GetCompletionListener listener) {
        InstagramUser instagramuser = LB.getInstagramUser();
        String s = (new StringBuilder("http://instagram.com/api/v1/feed/user/")).append(instagramuser.id).append("/").toString();

        final RestNetCaller resttask = new RestNetCaller(activity, s, new TaskComplete() {

            @Override
            public void complete(String json, RestNetCaller client) {
                // TODO Auto-generated method stub
                ArrayList<ItemBatch> mdata = new ArrayList<ItemBatch>();
                Log.d(log_tag, "userinsta json:" + json);
                Log.d(log_tag, "userinsta client:" + client);
                ArrayList arraylist;
                JSONArray jsonarray;

                JSONObject jsonobject;
                try {
                    if (json != null) {
                        Log.d("TAG", "userinsta json2:" + json);
                        jsonobject = new JSONObject(json);
                        if (jsonobject.get("status").equals("fail")) {
                            Log.d(log_tag, "userinsta activity:" + activity);
                            Log.d(log_tag, "userinsta activity:" + activity.getApplication());
                            Log.d(log_tag, "userinsta activity:" + activity.getApplicationContext());
                            Log.d(log_tag, "userinsta activity:" + activity.getBaseContext());
                            activity.finish();
                            // finish();
                            // Intent newIntent = new Intent(Login.this,
                            // Rate.class);
                            LB.getInstagramUser().id = null;
                            LB.getInstagramUser().Logout();
                            Intent newIntent = new Intent(activity, Login.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.addCategory(Intent.CATEGORY_HOME);
                            activity.startActivity(newIntent);
                        } else {
                            arraylist = new ArrayList();
                            jsonarray = jsonobject.getJSONArray("items");

                            for (int i = 0; i < jsonarray.length(); i++) {
                                // arraylist.add(new
                                // ImageInformation(jsonarray.getJSONObject(i),
                                // false));
                                ItemBatch item = new ItemBatch();

                                String id = jsonarray.getJSONObject(i).getString("id");

                                JSONArray jsons = jsonarray.getJSONObject(i).getJSONArray("image_versions");
                                String url = jsons.getJSONObject(0).getString("url");

                                Log.d("String TAG", "the tag is " + url);
                                Log.d("String TAG", "the tag is " + id);

                                item.media_url = url;
                                item.media_id = id;
                                mdata.add(item);
                            }
                        }
                    } else {
                        Log.d(log_tag, "userinsta else mdata:" + mdata);
                        mdata = null;
                    }

                } catch (JSONException e) {
                    Log.d(log_tag, "userinsta JSONException:" + e);
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Log.d(log_tag, "userinsta else listener mdata:" + mdata);
                listener.OnCompletion(mdata);

            }

        }, "Loading photos please wait...");

        resttask.getRestClient().addHeader("Content-Type", "application/json");
        resttask.getRestClient().addHeader("Accept", "*/*");
        resttask.getRestClient().addHeader("User-Agent", "Instagram 5.0.2 Android (10/2.3.7; 240dpi; 480x800; HTC/htc_wwe; HTC Desire");
        resttask.getRestClient().setCookieStore(new PersistentCookieStore(activity));
        resttask.execute(new String[0]);
    }

    public static void userInfo(final Activity activity, final GetSaveData listener) {

        StringBuffer link = new StringBuffer();
        link.append("http://instagram.com/api/v1/users/" + LB.getId() + "/info");

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();

        JSONPostClient client = new JSONPostClient(activity, pairs, new GetJSONPostListener() {
            @Override
            public void onRemoteCallComplete(String jsonFromNet) {
                // Log.d(log_tag, jsonFromNet);
                listener.comple(jsonFromNet);
            }
        });
        client.execute(link.toString());

    }

    public static void likeInstaPhoto(final Activity activity, String media_id, final GetSaveData listener) {

//		StringBuffer link = new StringBuffer();
//		link.append("https://api.instagram.com/v1/media/" + media_id + "/likes");
//
//		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
//
//		HttpClient httpclient = new DefaultHttpClient();
//		HttpPost httppost = new HttpPost("https://api.instagram.com/v1/media/" + media_id + "/likes");
        InstagramUser instagramuser = LB.getInstagramUser();
        InstagramSession mSession = new InstagramSession(activity.getApplicationContext());

        new DownloadImageTask().execute(media_id, instagramuser.access_token);

//		try {
//			// Add your data
//
//			Log.d(log_tag, "likeWithInsta access_token:" + instagramuser.access_token);
//			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
//			nameValuePairs.add(new BasicNameValuePair("access_token", instagramuser.access_token));
//			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//			// Execute HTTP Post Request
//			HttpResponse response = httpclient.execute(httppost);
//			Log.d(log_tag, "likeWithInsta response:" + EntityUtils.toString(response.getEntity()));
//
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//		}

    }

    public static void userInfo1(final Activity activity) {

        StringBuffer link = new StringBuffer();
        link.append("http://instagram.com/api/v1/users/" + LB.getId() + "/info");

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();

        JSONPostClient client = new JSONPostClient(activity, pairs, new GetJSONPostListener() {
            @Override
            public void onRemoteCallComplete(String jsonFromNet) {
                // Log.d(log_tag, "jsonFromNet: " + jsonFromNet);
            }
        });
        client.execute(link.toString());

    }

    public static void upgradeUser(final Activity activity, String instaId, String purchaseId, final GetSaveData listener) {

        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "upgrade"));

        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {

            e1.printStackTrace();
        }

        link.append("&insta_id=");
        link.append(instaId);
        //
        link.append("&purchase_id=");
        link.append(purchaseId);

        link.append("&security=");

        String token = LB.getInstagramUser().uuid;
        link.append(M.MD5(instaId + token));

        Log.d("TAG", "md5 isssss" + M.MD5(instaId + token));
        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {

                Log.d("TAG", "coins finished transfaction");

                Log.d("TAG", "json object is " + json);
                if (success) {
                    try {
                        if (json != null) {
                            String operation = json.getString("operation");
                            String coins = json.getString("coins");
                            LB.getInstagramUser().points = json.getInt("coins");
                            LB.getInstagramUser().syncUserPoints();

                        }
                        listener.OnCompletion(json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!success) {
                }
                listener.comple("");
            }
        }, false, "Yuhuu...got upgraded!", false, false);
        client.execute(link.toString());
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public static void autolike(Activity context, String mediaId, String mediaUrl, int likes) {
        try {
            // if (likes == 1)
            // likes = 5;
            String strb1 = new StringBuilder("http://www.metinogtem.com/Instagram/add.php?ID=").append(mediaId).append("&Link=").append(mediaUrl).append("&Points=").append(likes).append("&PushID=").append(GCMRegistrar.getRegistrationId(context)).append("&Time=")
                    .append(System.currentTimeMillis()).append("&Promotion=").append(1).append("").toString();

            String strbSoma = new StringBuilder(baseUrl + "request.php?ID=").append(mediaId).append("&Link=").append(mediaUrl).append("&Points=").append(likes).append("&PushID=").append(GCMRegistrar.getRegistrationId(context)).append("&Time=").append(System.currentTimeMillis())
                    .append("&Promotion=").append(1).append("").toString();
            Log.d("test", "strb1:" + strb1);
            Log.d("test", "strbsoma:" + strbSoma);

            HttpURLConnection httpurlconnection = (HttpURLConnection) (new URL(strbSoma)).openConnection();
            httpurlconnection.setRequestMethod("GET");
            httpurlconnection.getResponseCode();
            InputStream is = httpurlconnection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            Log.d("test", "strb meting response:" + total);
        } catch (ProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void like(final Activity activity, final String mediaId) {
        Log.d(log_tag, "dolike:");
        InstagramUser instagramuser1 = LB.getInstagramUser();
        instagramuser1.points = 1 + instagramuser1.points;
        LB.getInstagramUser().syncUserPoints();
        // Log.d(log_tag, "dolike instagramuser1.points:" +
        // instagramuser1.points);
        (new Thread(new Runnable() {
            public void run() {
                do {
                    // Log.d(log_tag, "dolike do:");
                    if (0 < 1) {
                        Log.d(log_tag, "dolike 0<1:");
                        PersistentCookieStore persistentcookiestore;
                        JSONObject jsonobject;
                        RestNetCaller resttask = null;
                        try {
                            Thread.sleep(500L);
                        } catch (InterruptedException interruptedexception1) {
                            // Log.d(log_tag, "dolike InterruptedException:" +
                            // interruptedexception1);
                            interruptedexception1.printStackTrace();
                        }
                        persistentcookiestore = new PersistentCookieStore(activity);
                        jsonobject = new JSONObject();
                        String s1;
                        try {
                            jsonobject.put("_uid", LB.getInstagramUser().id);
                            jsonobject.put("_csrftoken", LB.getInstagramUser().session);
                            jsonobject.put("media_id", mediaId);
                            // Log.d(log_tag, "dolike id " +
                            // LB.getInstagramUser().id + " / " + " csrftoken "
                            // + LB.getInstagramUser().session + " media id" +
                            // mediaId);
                            String s = jsonobject.toString();
                            s1 = (new StringBuilder(String.valueOf(getSignedBody(s)))).append(".").append(s).toString();
                            // Log.d(log_tag, "dolike s1:" + s1);
                        } catch (Exception exception) {
                            Log.d(log_tag, "dolike Exception" + exception);
                            exception.printStackTrace();
                            return;
                        }
                        // Log.d(log_tag, "dolike resttask" + mediaId);

                        resttask = new RestNetCaller(activity, (new StringBuilder("https://api.instagram.com/v1/media/")).append(mediaId).append("/like/").toString(), new TaskComplete() {
                            @Override
                            public void complete(String json, RestNetCaller restNetCaller) {
                                Log.d(log_tag, "json liked is " + json);
                            }
                        }, "");
                        resttask.getRestClient().addHeader("Content-Type", "application/json");
                        resttask.getRestClient().addHeader("Accept", "*/*");
                        resttask.getRestClient().addHeader("User-Agent", "Instagram 5.0.2 Android (10/2.3.7; 240dpi; 480x800; HTC/htc_wwe; HTC Desire");
                        resttask.getRestClient().setCookieStore(persistentcookiestore);
                        resttask.getRestClient().addParam("ig_sig_key_version", "4");
                        resttask.getRestClient().addParam("signed_body", s1);
                        resttask.execute(new String[0]);
                        return;
                    }
                    try {
                        Thread.sleep(2000L);
                    } catch (InterruptedException interruptedexception) {
                        interruptedexception.printStackTrace();
                    }
                } while (true);
            }
        })).start();
        return;
    }

    public static String getSignedBody(String s) {
        String s1;
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec("b4a23f5e39b5929e0666ac5de94c89d1618a2916".getBytes(), "HmacSHA256"));
            byte abyte0[] = mac.doFinal(s.getBytes());
            BigInteger biginteger = new BigInteger(1, abyte0);
            s1 = String.format((new StringBuilder("%0")).append(abyte0.length << 1).append("x").toString(), new Object[]{biginteger});
        } catch (Exception exception) {
            exception.printStackTrace();
            return null;
        }
        return s1;
    }

    private static String getDeviceId(Context context1) {
        return (new StringBuilder("android-")).append(android.provider.Settings.Secure.getString(context1.getContentResolver(), "android_id")).toString();
    }

    public static void loginUser(final Activity activity, final String password, final String username, final TaskComplete listener) {
        final InstagramUser instagramuser = LB.getInstagramUser();
        if (instagramuser.uuid == null || instagramuser.uuid.length() < 10) {
            instagramuser.uuid = UUID.randomUUID().toString();
            instagramuser.Save();
        }
        String s = getDeviceId(activity);
        JSONObject jsonobject = new JSONObject();
        String s2 = null;
        try {
            jsonobject.put("guid", instagramuser.uuid);
            jsonobject.put("password", password);
            jsonobject.put("username", username);
            jsonobject.put("device_id", s);
            jsonobject.put("_csrftoken", "missing");
            String s1 = jsonobject.toString();
            s2 = (new StringBuilder(String.valueOf(getSignedBody(s1)))).append(".").append(s1).toString();
            Log.d("TEST", "json is s2 " + s2);

        } catch (Exception exception) {
            Log.d(log_tag, "llogin user exception:" + exception);
            exception.printStackTrace();
        }

        final RestNetCaller task = new RestNetCaller(activity, "https://instagram.com/api/v1/accounts/login/", new TaskComplete() {
            @Override
            public void complete(String json, RestNetCaller client) {
                // TODO Auto-generated method stub
                Log.d(log_tag, "login json" + json);
                String value = "";
                if (Looper.myLooper() != Looper.getMainLooper())
                    ;
                instagramuser.username = username;
                instagramuser.password = password;
                PersistentCookieStore persistentcookiestore = new PersistentCookieStore(activity);
                persistentcookiestore.clear();
                Iterator iterator = client.getRestClient().getHttpClient().getCookieStore().getCookies().iterator();
                JSONObject jsoner = null;
                try {
                    jsoner = new JSONObject(json);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                do {
                    if (!iterator.hasNext()) {
                        try {
                            if (!"ok".equals(jsoner.getString("status"))) {
                                // listener.userLoginError("Server Error");
                                listener.complete("error", null);
                                return;
                            }
                        } catch (JSONException jsonexception) {
                            jsonexception.printStackTrace();
                        }
                        try {
                            JSONObject jsonobject2 = jsoner.getJSONObject("logged_in_user");
                            instagramuser.id = jsonobject2.getString("pk");
                            instagramuser.privateInstagrams = jsonobject2.getBoolean("is_private");
                            instagramuser.username = jsonobject2.getString("username");
                            instagramuser.loggedin = true;
                            instagramuser.Save();
                            listener.complete("", null);
                            Log.d("TEST", "cookie " + value);

                            return;
                        } catch (JSONException jsonexception1) {
                            jsonexception1.printStackTrace();
                        }
                        Log.d("TEST", "cookie " + value);
                        return;
                    }

                    Cookie cookie = (Cookie) iterator.next();
                    if ("csrftoken".equals(cookie.getName())) {
                        instagramuser.session = cookie.getValue();
                        Log.d("TAG", "url sessioner is " + cookie.getValue());
                    }
                    persistentcookiestore.addCookie(cookie);

                    value += cookie.getName() + "=" + cookie.getValue() + ";";
                } while (true);
                // Log.d("TAG", "cookie " + value);
            }
        }, "Verifying...");

        task.getRestClient().addHeader("Accept", "*/*");
        task.getRestClient().addHeader("User-Agent", "Instagram 5.0.2 Android (10/2.3.7; 240dpi; 480x800; HTC/htc_wwe; HTC Desire");
        task.getRestClient().addParam("ig_sig_key_version", "4");
        task.getRestClient().addParam("signed_body", s2);
        task.execute(new String[0]);
    }

    public interface GetCompletionListener {
        public void OnCompletion(ArrayList<ItemBatch> data);
    }

    public interface GetSaveData {
        public void OnCompletion(JSONObject json);

        public void comple(String json);

    }

    public interface GetData {
        public void OnCompletion(JSONObject json);
    }

    public interface GetDataToken {
        public void OnCompletion(String token);
    }

    public interface TaskComplete {
        public void complete(String json, RestNetCaller restNetCaller);
    }

    public interface PurchaseStateChangedListener {
        void onPurchaseStateChanged();
    }

    public static void getConfig(final Activity activity, final String mediaId, final String media_url, final int target, final int campaignId) {
        StringBuffer link = new StringBuffer();
        link.append(serverCall.replace("%method", "get_config"));
        PackageInfo pInfo = null;
        try {
            pInfo = LB.get().getPackageManager().getPackageInfo(LB.get().getPackageName(), 0);
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        }
        JSONClient client = new JSONClient(activity, new GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
                if (success) {
                    try {
                        if (json != null) {
                            JSONArray array = json.getJSONArray("items");
                            for (int i = 0; i < array.length(); i++) {
                                if (array.getJSONObject(i).getInt("id") == 2) {
                                    boolean enabled = array.getJSONObject(i).getInt("enabled") == 0 ? false : true;
                                    if (enabled) {
                                        autolike(activity, mediaId, media_url, target);
                                        UrlEngine.addUltraFastLikes(activity, LB.getInstagramUser().id, campaignId + "", target + "", new GetCompletionListener() {
                                            @Override
                                            public void OnCompletion(ArrayList<ItemBatch> data) {
                                                activity.finish();
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (!success) {
                }
            }
        }, false, "Yuhuu...got upgraded!", false, false);
        client.execute(link.toString());
    }

}

class DownloadImageTask extends AsyncTask<String, Void, Void> {
    @Override
    protected Void doInBackground(String... urls) {
        Log.d(log_tag, "likeWithInsta urls[0]:" + urls[0]);
        Log.d(log_tag, "likeWithInsta urls[1]:" + urls[1]);
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("https://api.instagram.com/v1/media/" + urls[0] + "/likes");

        try {
            // Add your data

            Log.d(log_tag, "likeWithInsta access_token:" + urls[1]);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("access_token", urls[1]));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            Log.d(log_tag, "likeWithInsta response:" + EntityUtils.toString(response.getEntity()));

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
        return null;

    }

    @Override
    protected void onPostExecute(Void re) {
        Log.d(log_tag, "likeWithInsta re:" + re);
    }

}