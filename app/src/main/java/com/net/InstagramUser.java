// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;

public class InstagramUser {

    private static final String PREFERENCE_NAME = "ig-user";
    public static InstagramUser staticUser;
    public Context context;
    public String id;
    public String lastPromoMediaId;
    public boolean loggedin;
    public String password;
    public int points;
    public int postCount;
    public int rated;
    public boolean privateInstagrams;
    public String session;
    public String username;
    public String uuid;
    public String access_token;

    public InstagramUser() {
        loggedin = false;
        postCount = 0;
        privateInstagrams = false;
        lastPromoMediaId = null;
    }

    public static InstagramUser getInstance(Context context1) {
        if (staticUser == null) {
            staticUser = new InstagramUser();
            staticUser.context = context1;
            staticUser = staticUser.Load();
        }
        return staticUser;
    }

    public InstagramUser Load() {
        SharedPreferences sharedpreferences = context.getSharedPreferences("ig-user", 0);
        uuid = sharedpreferences.getString("uuid", null);
        username = sharedpreferences.getString("username", null);
        access_token = sharedpreferences.getString("access_token", null);
        password = sharedpreferences.getString("password", null);
        id = sharedpreferences.getString("userId", null);
        session = sharedpreferences.getString("session", null);
        postCount = sharedpreferences.getInt("postCount", 0);
        privateInstagrams = sharedpreferences.getBoolean("privateInstagrams", false);
        lastPromoMediaId = sharedpreferences.getString("lastPromoMediaId", null);
        rated = sharedpreferences.getInt("rated", 0);
        getUserPoints();
        return this;
    }

    public InstagramUser Save() {
        android.content.SharedPreferences.Editor editor = context.getSharedPreferences("ig-user", 0).edit();
        editor.putString("uuid", uuid);
        editor.putString("username", username);
        editor.putString("password", password);
        editor.putString("access_token", access_token);
        editor.putString("userId", id);
        editor.putString("session", session);
        editor.putInt("postCount", postCount);
        editor.putInt("rated", rated);
        editor.putBoolean("privateInstagrams", privateInstagrams);
        editor.putString("lastPromoMediaId", lastPromoMediaId);
        editor.commit();
        return this;
    }

    public InstagramUser Logout() {

        android.content.SharedPreferences.Editor editor = context.getSharedPreferences("ig-user", 0).edit();
        editor.putString("uuid", null);
        editor.putString("username", null);
        editor.putString("password", null);
        editor.putString("access_token", null);
        editor.putString("userId", null);
        editor.putString("session", null);

        editor.putInt("postCount", 0);
//		editor.putInt("rated", 0);
        editor.putBoolean("privateInstagrams", false);
        editor.putString("lastPromoMediaId", null);
        editor.commit();
        return this;
    }

    public String getUserPoints() {
        if (Looper.myLooper() != Looper.getMainLooper())
            ;
        points = context.getSharedPreferences("UserPoints", 0).getInt("UserPoints", 50);
        return points + "";
    }

    public void syncUserPoints() {
        android.content.SharedPreferences.Editor editor = context.getSharedPreferences("UserPoints", 0).edit();
        editor.putInt("UserPoints", points);
        editor.commit();
    }

}
