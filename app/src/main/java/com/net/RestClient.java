// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.net;

import android.content.Context;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Iterator;

// Referenced classes of package com.film.instaliker.aouth.privateApi.rest:
//            MySSLSocketFactory

public class RestClient {

    private boolean authentication;
    private DefaultHttpClient client;
    protected Context context;
    private CookieStore cookieStore;
    MultiPartEntityExt.ProgressListener fileUploadProgressListener;
    private ArrayList headers;
    private String jsonBody;
    private String message;
    private ArrayList params;
    private String password;
    private String response;
    private int responseCode;
    private File uploadFile;
    private String url;
    private String username;

    public RestClient(String s) {
        cookieStore = null;
        url = s;
        params = new ArrayList();
        headers = new ArrayList();
    }

    private HttpUriRequest addBodyParams(HttpUriRequest request)
            throws Exception {
        if (jsonBody != null) {
            request.addHeader("Content-Type", "application/json");
            if (request instanceof HttpPost)
                ((HttpPost) request).setEntity(new StringEntity(jsonBody,
                        "UTF-8"));
            else if (request instanceof HttpPut)
                ((HttpPut) request).setEntity(new StringEntity(jsonBody,
                        "UTF-8"));

        } else if (!params.isEmpty()) {
            if (request instanceof HttpPost)
                ((HttpPost) request).setEntity(new UrlEncodedFormEntity(params,
                        HTTP.UTF_8));
            else if (request instanceof HttpPut)
                ((HttpPut) request).setEntity(new UrlEncodedFormEntity(params,
                        HTTP.UTF_8));
        }
        return request;
    }

    private String addGetParams() throws Exception {
        StringBuffer stringbuffer = new StringBuffer();
        if (params.isEmpty()) {
            return stringbuffer.toString();
        }
        Iterator iterator;
        stringbuffer.append("?");
        iterator = params.iterator();

        if (iterator.hasNext()) {
            NameValuePair namevaluepair = (NameValuePair) iterator.next();
            String s;
            if (stringbuffer.length() > 1) {
                s = "&";
            } else {
                s = "";
            }
            stringbuffer
                    .append((new StringBuilder(String.valueOf(s)))
                            .append(namevaluepair.getName())
                            .append("=")
                            .append(URLEncoder.encode(namevaluepair.getValue(),
                                    "UTF-8")).toString());
        }

        return stringbuffer.toString();
    }

    private HttpUriRequest addHeaderParams(HttpUriRequest httpurirequest)
            throws Exception {
        Iterator iterator = headers.iterator();
        do {
            if (!iterator.hasNext()) {
                if (authentication) {
                    UsernamePasswordCredentials usernamepasswordcredentials = new UsernamePasswordCredentials(
                            username, password);
                    httpurirequest.addHeader((new BasicScheme()).authenticate(
                            usernamepasswordcredentials, httpurirequest));
                }
                return httpurirequest;
            }
            NameValuePair namevaluepair = (NameValuePair) iterator.next();
            httpurirequest.addHeader(namevaluepair.getName(),
                    namevaluepair.getValue());
        } while (true);
    }

    private String convertStreamToString(InputStream is) {
        ByteArrayOutputStream oas = new ByteArrayOutputStream();
        copyStream(is, oas);
        String t = oas.toString();
        try {
            oas.close();
            oas = null;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return t;
    }

    private void copyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    private void executeRequest(HttpUriRequest request, String url) {
        client = getNewHttpClient();

        HttpResponse httpResponse;

        try {
            httpResponse = client.execute(request);
            responseCode = httpResponse.getStatusLine().getStatusCode();
            message = httpResponse.getStatusLine().getReasonPhrase();

            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {

                InputStream instream = entity.getContent();
                response = convertStreamToString(instream);

                // Closing the input stream will trigger connection release
                instream.close();
            }

        } catch (ClientProtocolException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        } catch (IOException e) {
            client.getConnectionManager().shutdown();
            e.printStackTrace();
        }
    }

    public void addBasicAuthentication(String s, String s1) {
        authentication = true;
        username = s;
        password = s1;
    }

    public void addHeader(String s, String s1) {
        headers.add(new BasicNameValuePair(s, s1));
    }

    public void addParam(String s, String s1) {
        params.add(new BasicNameValuePair(s, s1));
    }

    public void execute(int id) throws Exception {
        switch (id) {
            default:
                return;

            case 2: // '\002'
                executeRequest(
                        (HttpGet) addHeaderParams(new HttpGet((new StringBuilder(
                                String.valueOf(url))).append(addGetParams())
                                .toString())), url);
                return;

            case 3: // '\003'
                executeRequest(
                        (HttpPost) addBodyParams((HttpPost) addHeaderParams(new HttpPost(
                                url))), url);
                return;

            case 4: // '\004'
                executeRequest(
                        (HttpPut) addBodyParams((HttpPut) addHeaderParams(new HttpPut(
                                url))), url);
                return;

            case 1: // '\001'
                executeRequest((HttpDelete) addHeaderParams(new HttpDelete(url)),
                        url);
                return;
        }
    }

    public String getErrorMessage() {
        return message;
    }

    public DefaultHttpClient getHttpClient() {
        return client;
    }

    public DefaultHttpClient getNewHttpClient() {
        DefaultHttpClient defaulthttpclient;
        try {
            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            keystore.load(null, null);
            MySSLSocketFactory mysslsocketfactory = new MySSLSocketFactory(
                    keystore);
            mysslsocketfactory
                    .setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            BasicHttpParams basichttpparams = new BasicHttpParams();
            HttpProtocolParams
                    .setVersion(basichttpparams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(basichttpparams, "UTF-8");
            SchemeRegistry schemeregistry = new SchemeRegistry();
            schemeregistry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            schemeregistry
                    .register(new Scheme("https", mysslsocketfactory, 443));
            defaulthttpclient = new DefaultHttpClient(
                    new ThreadSafeClientConnManager(basichttpparams,
                            schemeregistry), basichttpparams);
            if (cookieStore != null) {
                defaulthttpclient.setCookieStore(cookieStore);
            }
        } catch (Exception exception) {
            return new DefaultHttpClient();
        }
        return defaulthttpclient;
    }

    public String getResponse() {
        return response;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public File getUploadFile() {
        return uploadFile;
    }

    public void setContext(Context context1) {
        context = context1;
    }

    public void setCookieStore(CookieStore cookiestore) {
        cookieStore = cookiestore;
    }

    public void setJSONString(String s) {
        jsonBody = s;
    }

    public void setParams(ArrayList arraylist) {
        params.addAll(arraylist);
    }

    public void setUploadFile(File file,
                              MultiPartEntityExt.ProgressListener progresslistener) {
        uploadFile = file;
        fileUploadProgressListener = progresslistener;
    }

    public void terminateConnection() {
        if (client != null) {
            client.getConnectionManager().shutdown();
        }
    }
}
