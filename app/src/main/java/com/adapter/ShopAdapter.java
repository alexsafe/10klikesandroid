package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.LightingColorFilter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.billing.v3.IabHelper;
import com.getmorelikes.pro.R;

import java.util.ArrayList;

public class ShopAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    static final int RC_REQUEST = 10001;
    ArrayList<String> data = new ArrayList<String>();
    String mediaId;
    String media_url;
    Activity activity;
    IabHelper mhelper;
    IabHelper.OnIabPurchaseFinishedListener listener;

    public ShopAdapter(Activity activity, ArrayList<String> data, IabHelper mhelper, IabHelper.OnIabPurchaseFinishedListener listener) {
        this.data = data;
        this.media_url = media_url;
        this.mediaId = mediaId;
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mhelper = mhelper;
        this.listener = listener;
    }

    /**
     * @param convertView The old view to overwrite, if one is passed
     * @returns a ContactEntryView that holds wraps around an ContactEntry
     */
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;

        if (convertView == null) {
            vi = inflater.inflate(R.layout.activity_shop_item1, null);
            holder = new ViewHolder();
            holder.likes = (TextView) vi.findViewById(R.id.likesTxt);
            holder.coinButton = (Button) vi.findViewById(R.id.buyButton);
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (position == 0) {
            holder.likes.setText("$0.99");
            holder.coinButton.setText("Get 50 coins");

        } else if (position == 1) {
            holder.likes.setText("$3.99");
            holder.coinButton.setText("Get 500 coins");

        } else if (position == 2) {
            holder.likes.setText("$8.99");
            holder.coinButton.setText("Get 1000 coins");

        } else if (position == 3) {
            holder.likes.setText("$18.99");
            holder.coinButton.setText("Get 5000 coins");

        } else if (position == 4) {
            holder.likes.setText("$26.99");
            holder.coinButton.setText("Get 10000 coins ");

        }

        holder.coinButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                // TODO Auto-generated method stub
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    v.getBackground().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFFAA0000));
                } else if (e.getAction() == MotionEvent.ACTION_UP) {
                    if (position == 0) {
                        // if (BillingHelper.isBillingSupported()) {
                        // BillingHelper.requestPurchase(activity,
                        // "android.test.purchased");
                        // } else {
                        // holder.coinButton.setEnabled(false); // XXX press
                        // button
                        // }
                        mhelper.launchPurchaseFlow(activity, "c_1", IabHelper.ITEM_TYPE_INAPP, RC_REQUEST, listener, "");

                    } else if (position == 1) {

                        mhelper.launchPurchaseFlow(activity, "c_2", IabHelper.ITEM_TYPE_INAPP, RC_REQUEST, listener, "");
                    } else if (position == 2) {
                        // if (BillingHelper.isBillingSupported()) {
                        // BillingHelper.requestPurchase(activity,
                        // "c_3");
                        // } else {
                        // holder.coinButton.setEnabled(false); // XXX press
                        // button
                        // }
                        mhelper.launchPurchaseFlow(activity, "c_3", IabHelper.ITEM_TYPE_INAPP, RC_REQUEST, listener, "");
                    } else if (position == 3) {
                        // if (BillingHelper.isBillingSupported()) {
                        // BillingHelper.requestPurchase(activity,
                        // "c_4");
                        // } else {
                        // holder.coinButton.setEnabled(false); // XXX press
                        // button
                        // }
                        mhelper.launchPurchaseFlow(activity, "c_4", IabHelper.ITEM_TYPE_INAPP, RC_REQUEST, listener, "");
                    } else if (position == 4) {
                        // if (BillingHelper.isBillingSupported()) {
                        // BillingHelper.requestPurchase(activity,
                        // "c_5");
                        // } else {
                        // holder.coinButton.setEnabled(false); // XXX press
                        // button
                        // }
                        mhelper.launchPurchaseFlow(activity, "c_5", IabHelper.ITEM_TYPE_INAPP, RC_REQUEST, listener, "");
                    }

                    v.getBackground().clearColorFilter();

                }
                return false;

            }
        });

        return vi;
    }

    public static class ViewHolder {
        TextView likes;
        Button coinButton;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

}
