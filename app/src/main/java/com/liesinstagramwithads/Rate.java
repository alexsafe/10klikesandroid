package com.liesinstagramwithads;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.adapter.ViewPagerAdapter;
import com.datasets.ItemBatch;
import com.getmorelikes.pro.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.liesinstagramwithads.UrlEngine.GetCompletionListener;
import com.liesinstagramwithads.UrlEngine.GetSaveData;
import com.net.MImageLoader;
import com.util.AppRater;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import static com.util.Config.log_tag;

public class Rate extends Fragment {

    int curentIndex = 0;
    TextView coins;
    boolean scrolling;
    boolean loadingMore;
    MImageLoader loader;
    TextView username, username1;
    ImageView image1, image2;
    ViewAnimator flipper;
    Handler handler;
    Runnable runnable;
    InterstitialAd interstitial;
    View view;
    String[] rank;
    String[] country;
    String[] population;
    int[] flag;
    ViewPager viewPager;
    PagerAdapter adapter;
    int selectedIndex = 0;
    int oldIndex = 0;
    int swipe = 0;
    float mStartDragX;
    Button skipRate;
    Button likePictureRate;

    boolean mPageEnd;
    float downX;
    float upX;

    String PModal;
    String PRate;
    int rated;

    @SuppressLint("NewApi")
    // protected void onCreate(Bundle savedInstanceState) {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(log_tag, "create rate");
        // super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Get a Tracker (should auto-report)
        // Tracker app_tracker = ((LB)
        // Rate.this.getActivity().getApplication()).getTracker(LB.TrackerName.APP_TRACKER);
        // Tracker global_tracker = ((LB)
        // this.getActivity().getApplication()).getTracker(LB.TrackerName.GLOBAL_TRACKER);
        // app_tracker.setScreenName("Win coins");
        // app_tracker.send(new HitBuilders.AppViewBuilder().build());
        // global_tracker.send(new HitBuilders.AppViewBuilder().build());
        view = inflater.inflate(R.layout.viewpager_main, container, false);
        loadNewItems(0);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        skipRate = (Button) view.findViewById(R.id.skipRate);
        likePictureRate = (Button) view.findViewById(R.id.likePictureRate);
        skipRate.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                loadNewItems(1);
                // skip();
                swipe = 1;

            }
        });
        likePictureRate.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                loadNewItems(2);
                // like();
                swipe = 2;

            }
        });
        mStartDragX = 0;
        final int SWIPE_MIN_DISTANCE = 100;
        viewPager.setOnTouchListener(new ViewPager.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float x = event.getX();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downX = event.getX();
                        return false;
                    case MotionEvent.ACTION_MOVE:

                        upX = event.getX();
                        final float deltaX = downX - upX;
                        if (deltaX < 0 && -deltaX > SWIPE_MIN_DISTANCE) {
                            // DETECTED SWIPE TO LEFT
                            swipe = 2;
                        }
                        if (deltaX > 0 && deltaX > SWIPE_MIN_DISTANCE) {
                            // DETECTED SWIPE TO RIGHT
                            swipe = 1;
                        }
                        return false;
                }
                return false;
            }
        });

        OnPageChangeListener mListener = new OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                // TODO Auto-generated method stub
                selectedIndex = arg0;
                oldIndex = arg0;
                // System.out.println("Selecte Page: " + selectedIndex);
                Log.d(log_tag, "onPageSelected Selecte Page: " + selectedIndex);
                Log.d(log_tag, "onPageSelected Selecte Page swipe: " + swipe);
                if (swipe != 0)
                    loadNewItems(swipe);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // Log.d(log_tag,"onPageScrolled");
                // TODO Auto-generated method stub
                if (mPageEnd && arg0 == selectedIndex) {
                    Log.d(getClass().getName(), "Okay");
                    mPageEnd = false;// To avoid multiple calls.
                } else {
                    mPageEnd = false;
                }
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub
                if (selectedIndex == adapter.getCount() - 1) {
                    mPageEnd = true;
                    Log.d(log_tag, "onPageScrollStateChanged");
                }
            }
        };
        viewPager.setOnPageChangeListener(mListener);
        return view;
        // createInterstitial();
    }

    // ArrayList<ItemBatch> itemes = new ArrayList<ItemBatch>();
    // int index = 0;

    public void doSkip() {
        skip();
        loadNewItems(1);
    }

    public void doLike() {
        final Toast toast = Toast.makeText(Rate.this.getActivity().getApplicationContext(), "Liked", Toast.LENGTH_LONG);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 200);
        like();
        loadNewItems(2);
    }

    private void loadNewItems(final int state) {
        // itemes.clear();
        Log.d(log_tag, "in loadNewItems state:" + state);
        if (state == 0) {
            UrlEngine.getItemsBatch(Rate.this.getActivity(), LB.getInstagramUser().id, new GetCompletionListener() {
                @Override
                public void OnCompletion(ArrayList<ItemBatch> data) {
                    Log.d(log_tag, "loadNewItemsselect :" + data);
                    Log.d(log_tag, "loadNewItemsselect state :" + state);
                    if (data == null) {
                        Log.d(log_tag, "loadNewItemsselect completion data null");
                        // Button retry = new Button(getActivity());
                        // retry.setText("Retry");
                        // ((ViewGroup) view).addView(retry);
                        // showEditDialog();
                        AlertDialog ad = new AlertDialog.Builder(getActivity())

                                // Set Dialog Title
                                .setTitle("Server error")
                                // Set Dialog Message
                                .setMessage("Click retry")

                                // Positive button
                                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Do something else
                                        Log.d(log_tag, "clicked ok");
                                        loadNewItems(0);
                                    }
                                })

                                // Negative Button
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Log.d(log_tag, "clicked cancel");
                                        // Do something else
                                    }
                                }).create();
                        ad.show();
                    } else {
                        for (int i = 0; i < 3; i++) {
                            // Log.d(log_tag, "completion item batch url:" +
                            // data.get(i).media_url);
                            // itemes.add(data.get(i));
                            if (data.size() > 0) {
                                if (i < data.size())
                                    itemes.add(data.get(i));
                                else
                                    itemes.add(data.get(data.size() - 1));
                            }
                        }
                        Log.d(log_tag, "loadNewItems itemes.size():" + itemes.size());
                        for (int i = 0; i < itemes.size(); i++) {
                            Log.d(log_tag, "loadNewItems prepareImages item campaign_id:" + i + " " + itemes.get(i).media_id);

                        }

                        adapter = new ViewPagerAdapter(Rate.this.getActivity(), itemes, Rate.this.getActivity());
                        viewPager.setAdapter(adapter);
                        viewPager.setCurrentItem(1);
                    }
                }
            });
        } else {
            if (state == 1) {
                skip();
            }
            if (state == 2) {
                like();
            }
            UrlEngine.getItemsBatch(Rate.this.getActivity(), LB.getInstagramUser().id, new GetCompletionListener() {
                @Override
                public void OnCompletion(ArrayList<ItemBatch> data) {
                    Log.d(log_tag, "else completion item batch test :" + data);
                    Log.d(log_tag, "else itemes :" + itemes.size());
                    if (state == 1) {
                        // skip();
                        if (itemes.size() > 1) {
                            itemes.remove(1);
                            itemes.remove(1);
                            for (int i = 0; i < 2; i++) {
                                if (data.size() > 0) {
                                    if (i < data.size())
                                        itemes.add(data.get(i));
                                    else
                                        itemes.add(data.get(data.size() - 1));
                                }

                            }
                        }

                    }
                    if (state == 2) {
                        // like();
                        if (itemes.size() > 1) {
                            itemes.remove(0);
                            itemes.remove(0);
                            for (int i = 0; i < 2; i++) {
                                if (data.size() > 0) {
                                    if (i < data.size())
                                        itemes.add(data.get(i));
                                    else
                                        itemes.add(data.get(data.size() - 1));
                                }
                            }
                        }
                        // Collections.swap(itemes, 1, 0);
                    }
                    Log.d(log_tag, "else loadNewItems itemes.size():" + itemes.size());
                    for (int i = 0; i < itemes.size(); i++) {
                        Log.d(log_tag, "else loadNewItems prepareImages item campaign_id:" + i + " " + itemes.get(i).media_id);

                    }
                    Collections.swap(itemes, 1, 0);
                    // adapter = new ViewPagerAdapter(Rate.this.getActivity(),
                    // itemes, Rate.this.getActivity());
                    // viewPager.setAdapter(adapter);
                    swipe = 0;
                    // viewPager.setCurrentItem(1);
                    viewPager.setAdapter(adapter);
                    viewPager.setCurrentItem(1);
                }
            });
        }

    }

    public void skip() {
        if (itemes.size() > 0 && 1 < itemes.size()) {
            UrlEngine.respondItem(Rate.this.getActivity(), LB.getInstagramUser().id, itemes.get(1).campaign_id, "0", new GetSaveData() {
                @Override
                public void OnCompletion(JSONObject json) {
                    saveLikes();
                }

                @Override
                public void comple(String json) {
                    saveLikes();
                }
            }, true);
        }
    }

    public void like() {
        // Log.d(log_tag, "itemes like");
        if (itemes.size() > 0) {
            final Toast toast = Toast.makeText(Rate.this.getActivity().getApplicationContext(), "Liked", Toast.LENGTH_LONG);
            toast.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                }
            }, 200);
            for (int i = 0; i < itemes.size(); i++) {
                Log.d(log_tag, "likeWithInsta get index.media_id:" + itemes.get(i).media_id);
                Log.d(log_tag, "likeWithInsta get index.media_url:" + itemes.get(i).media_url);
                // Log.d(log_tag, "itemes get index.campaign_id " + i + " :" +
                // itemes.get(i).campaign_id);
            }
            Log.d(log_tag, "itemes get index:" + index);
            Log.d(log_tag, "likeWithInsta index.media_id:" + itemes.get(1).media_id);
            // Log.d(log_tag, "itemes get index.campaign_id:" +
            // itemes.get(index_to_use).campaign_id);
            UrlEngine.likeInstaPhoto(Rate.this.getActivity(), itemes.get(1).media_id, new GetSaveData() {

                @Override
                public void comple(String json) {
                    // TODO Auto-generated method stub
                    Log.d(log_tag, "likeWithInsta json:" + json);
                }

                @Override
                public void OnCompletion(JSONObject json) {
                    // TODO Auto-generated method stub
                    Log.d(log_tag, "likeWithInsta json:" + json);
                }
            });

            if (1 < itemes.size())
                UrlEngine.respondItem(Rate.this.getActivity(), LB.getInstagramUser().id, itemes.get(1).campaign_id, "1", new GetSaveData() {
                    @Override
                    public void OnCompletion(JSONObject json) {
                        // coins.setText(LB.getInstagramUser().getUserPoints()
                        // + "");
                        // Log.d(log_tag, "completion like" +
                        // LB.getInstagramUser().getUserPoints());
                        // Log.d(log_tag, "completion like json" + json);
                        saveLikes();
                        index = 0;
                        try {
                            if (json != null) {
                                if (json.getString("autolike").equals("true")) {
                                    UrlEngine.like(Rate.this.getActivity(), itemes.get(1).media_id);
                                }
                            }
                        } catch (JSONException e) {
                            Log.d(log_tag, "respondItem JSONException : " + e);
                            e.printStackTrace();
                        }
                        // createModal();
                    }

                    @Override
                    public void comple(String json) {
                        saveLikes();
                    }
                }, true);
            // loadNewItems(2);
            // createInterstitial();
        }
    }

    public void createModal() {
        SharedPreferences sharedpreferences = Rate.this.getActivity().getApplicationContext().getSharedPreferences("ig-user", 0);
        String PModal = sharedpreferences.getString("PModal", null);
        String PRate = sharedpreferences.getString("PRate", null);
        int rated = sharedpreferences.getInt("rated", 0);
        // int prob =
        // Integer.parseInt(getResources().getString(R.string.prob_like));
        int prob = Integer.parseInt(PModal);
        int probRate = Integer.parseInt(PRate);
        int probModal = Integer.parseInt(PModal);
        int check = (int) (Math.random() * 100);
        // Log.d(log_tag, "prob: " + check);
        // Log.d(log_tag, "prob PModal: " + PModal);
        // Log.d(log_tag, "prob probRate: " + probRate);
        // Log.d(log_tag, "rated:" + rated);
        // AppRater.app_launched(Rate.this.getActivity());
        if (check < probModal) {
            int checkRate = (int) (Math.random() * 100);
            if (checkRate < probRate) {
                if (rated != 1) {
                    AppRater.app_launched(Rate.this.getActivity());
                }
            }
        }
    }

    public void createInterstitial() {
        Log.d(log_tag, "createInterstitial");
        final SharedPreferences sharedpreferences = Rate.this.getActivity().getApplicationContext().getSharedPreferences("ig-user", 0);
        String adMobId = sharedpreferences.getString("adMobId", null);
        String adInterstitialId = getResources().getString(R.string.interstitialId);
        // Create the interstitial.
        interstitial = new InterstitialAd(Rate.this.getActivity());
        // interstitial.setAdUnitId("ca-app-pub-3307731116933970/4020211244");
        // old pub
        interstitial.setAdUnitId(adInterstitialId);
        // somadev soma12social
        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();
        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                Log.d(log_tag, "adloaded");

                String PModal = sharedpreferences.getString("PModal", null);
                String PRate = sharedpreferences.getString("PRate", null);
                int rated = sharedpreferences.getInt("rated", 0);
                // int prob =
                // Integer.parseInt(getResources().getString(R.string.prob_like));
                int prob = Integer.parseInt(PModal);
                int probRate = Integer.parseInt(PRate);
                int check = (int) (Math.random() * 100);
                Log.d(log_tag, "prob: " + check);
                Log.d(log_tag, "prob PModal: " + prob);
                Log.d(log_tag, "prob PModal: " + probRate);
                if (check < prob) {
                    displayInterstitial();
                    // if (rated != 1) {
                    //
                    // AppRater.app_launched(Rate.this.getActivity());
                    //
                    // }
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Log.d(log_tag, "onstart rate analitics");
        // Get an Analytics tracker to report app starts & uncaught exceptions
        // etc.
        // GoogleAnalytics.getInstance(this).reportActivityStart(this);
        GoogleAnalytics.getInstance(Rate.this.getActivity().getApplicationContext()).reportActivityStart(this.getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        interstitial = null;
        if (interstitial != null) {
            if (interstitial.isLoaded()) {
                interstitial = null;
            }
        }
        // Stop the analytics tracking
        // GoogleAnalytics.getInstance(this).reportActivityStop(this);
        GoogleAnalytics.getInstance(Rate.this.getActivity().getApplicationContext()).reportActivityStop(this.getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        interstitial = null;
        // Stop the analytics tracking
        // GoogleAnalytics.getInstance(this).reportActivityStop(this);
        GoogleAnalytics.getInstance(Rate.this.getActivity().getApplicationContext()).reportActivityStop(this.getActivity());
    }

    // Invoke displayInterstitial() when you are ready to display an
    // interstitial.
    public void displayInterstitial() {
        if (interstitial != null) {
            if (interstitial.isLoaded()) {
                interstitial.show();
            }
        }
    }

    ArrayList<ItemBatch> itemes = new ArrayList<ItemBatch>();
    int index = 0;

    private void loadNewItems() {
        Log.d("TAG", "loadNewItems");
        UrlEngine.getItemBatch(Rate.this.getActivity(), LB.getInstagramUser().id, new GetCompletionListener() {
            @Override
            public void OnCompletion(ArrayList<ItemBatch> data) {
                Log.d(log_tag, "completion item batch");
                itemes = new ArrayList<ItemBatch>();
                itemes.addAll(data);
                index = 0;
                prepareImages();
            }
        });
    }

    private void prepareImages() {

        loader.DisplayImage(itemes.get(index).media_url, Rate.this.getActivity(), image1);
        image1.setTag(itemes.get(index).media_url);
    }

    private void initUI() {

        // coins = (TextView) view.findViewById(R.id.coins);
        //
        // coins.setOnClickListener(new Button.OnClickListener() {
        // @Override
        // public void onClick(View v) {
        // Intent intent = new Intent(Rate.this.getActivity(), Shop.class);
        // startActivity(intent);
        // }
        // });
        //
        // TextView t1 = (TextView) view.findViewById(R.id.title1);
        //
        // ToggleButton likesButton = (ToggleButton)
        // view.findViewById(R.id.coinsButton);
        // likesButton.setChecked(true);
        // likesButton.setEnabled(false);
        // ToggleButton coinsButton = (ToggleButton)
        // view.findViewById(R.id.likesButton);
        // coinsButton.setOnCheckedChangeListener(new OnCheckedChangeListener()
        // {
        // @Override
        // public void onCheckedChanged(CompoundButton toggleButton, boolean
        // isChecked) {
        // Intent intent = new Intent(Rate.this.getActivity(), Likes.class);
        // startActivity(intent);
        // }
        // });
    }

    public void saveLikes() {
        SharedPreferences sharedPrefs = getActivity().getApplicationContext().getSharedPreferences("ig-user", 0);
        Editor editor11 = sharedPrefs.edit();
        editor11.putString("likes", LB.getInstagramUser().getUserPoints() + "");
        editor11.commit();
        registerForContextMenu(view);
        if (Rate.this.getActivity() != null)
            Rate.this.getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TAG", "onresume");
        // final TextView coins = (TextView) view.findViewById(R.id.coins);
        // coins.setText(LB.getInstagramUser().getUserPoints() + "");

        getActivity().getActionBar().setTitle(R.string.win_coins);

    }
}
