package com.net;

// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class MultiPartEntityExt extends MultipartEntity {
    public static class CountingOutputStream extends FilterOutputStream {

        private final ProgressListener listener;
        private long totalContentSize;
        private long transferred;

        public void write(int i)
                throws IOException {
            out.write(i);
            transferred = 1L + transferred;
            if (listener != null) {
                listener.transferred(totalContentSize, transferred);
            }
        }

        public void write(byte abyte0[], int i, int j)
                throws IOException {
            out.write(abyte0, i, j);
            transferred = transferred + (long) j;
            if (listener != null) {
                listener.transferred(totalContentSize, transferred);
            }
        }

        public CountingOutputStream(OutputStream outputstream, ProgressListener progresslistener, long l) {
            super(outputstream);
            listener = progresslistener;
            transferred = 0L;
            totalContentSize = l;
        }
    }

    public static interface ProgressListener {

        public abstract void transferred(long l, long l1);
    }


    private ProgressListener listener;

    public MultiPartEntityExt(ProgressListener progresslistener) {
        listener = progresslistener;
    }

    public MultiPartEntityExt(HttpMultipartMode httpmultipartmode, ProgressListener progresslistener) {
        super(httpmultipartmode);
        listener = progresslistener;
    }

    public MultiPartEntityExt(HttpMultipartMode httpmultipartmode, String s, Charset charset, ProgressListener progresslistener) {
        super(httpmultipartmode, s, charset);
        listener = progresslistener;
    }

    public void setProgressListener(ProgressListener progresslistener) {
        listener = progresslistener;
    }

    public void writeTo(OutputStream outputstream)
            throws IOException {
        super.writeTo(new CountingOutputStream(outputstream, listener, getContentLength()));
    }
}
