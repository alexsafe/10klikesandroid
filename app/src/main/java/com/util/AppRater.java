package com.util;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RatingBar;
import android.widget.TextView;

import com.getmorelikes.pro.R;

import static com.util.Config.log_tag;

public class AppRater {
    private final static String APP_TITLE = "Likes for Instagram";
    private final static String APP_PNAME = "com.noadslikesoninstagrampro";

    private final static int DAYS_UNTIL_PROMPT = 1;
    private final static int LAUNCHES_UNTIL_PROMPT = 3;

    public static void app_launched(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) {
            return;
        }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        // if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
        // if (System.currentTimeMillis() >= date_firstLaunch +
        // (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
        //
        // }
        // }
        showRateDialog(mContext, editor);
        editor.commit();
    }

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitle(mContext.getResources().getString(R.string.rate_title) + "?");
        dialog.getWindow().setLayout(275, 650);
        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams paramsll = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        // paramsll.height=700;
        ll.setLayoutParams(paramsll);

        RatingBar rating = new RatingBar(mContext);
        rating.setIsIndicator(true);
        rating.setFocusable(false);
        LinearLayout.LayoutParams paramsRating = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        paramsRating.gravity = Gravity.CENTER_HORIZONTAL;
        rating.setLayoutParams(paramsRating);

        LayerDrawable stars = (LayerDrawable) rating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        // rating.setNumStars(5);
        rating.setRating(5);

        ll.addView(rating);
        TextView tv = new TextView(mContext);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params1.setMargins(30, 15, 30, 45);

        tv.setLayoutParams(params1);
        tv.setText(mContext.getResources().getString(R.string.rate_body) + "!");
        ll.addView(tv);

        Button b1 = new Button(mContext);
        LinearLayout.LayoutParams paramsb1 = new LinearLayout.LayoutParams(280, 80);
        paramsb1.gravity = Gravity.CENTER_HORIZONTAL;
        paramsb1.bottomMargin = 15;
        // Button b1 = (Button) getLayoutInflater().inflate(R.layout., null);
        b1.setLayoutParams(paramsb1);
        b1.setText("Rate " + APP_TITLE);
        b1.setText(mContext.getResources().getString(R.string.rate_button));
        b1.setTextColor(Color.WHITE);
        b1.setWidth(280);
        b1.setBackgroundResource(R.drawable.custom_button);
        b1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                SharedPreferences sharedpreferences = mContext.getSharedPreferences("ig-user", 0);
                String ratePackage = sharedpreferences.getString("ratePackage", APP_PNAME);
                Log.d(log_tag, "apprater ratePackage: " + ratePackage);
                android.content.SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("rated", 1);
                editor.commit();
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + ratePackage)));

                dialog.dismiss();
            }
        });

        ll.addView(b1);

//		TextView skip = new TextView(mContext);
//		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
//		// skip.setWidth(170);
//		// params.gravity = Gravity.TOP;
//		params.setMargins(5, 15, 30, 10);
//
//		skip.setLayoutParams(params);
//		skip.setText(mContext.getResources().getString(R.string.skip));
//		skip.setGravity(Gravity.BOTTOM);
//
//		skip.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				// if (editor != null) {
//				// editor.putBoolean("dontshowagain", true);
//				// editor.commit();
//				// }
//				dialog.dismiss();
//			}
//		});
//		ll.addView(skip);

        Button b3 = new Button(mContext);
        LinearLayout.LayoutParams paramsb3 = new LinearLayout.LayoutParams(280, 80);
        paramsb3.gravity = Gravity.CENTER_HORIZONTAL;
        paramsb3.bottomMargin = 15;
        // Button b1 = (Button) getLayoutInflater().inflate(R.layout., null);
        b3.setLayoutParams(paramsb3);

        b3.setText(mContext.getResources().getString(R.string.rate_skip));
        b3.setTextColor(Color.WHITE);
        b3.setWidth(280);
        b3.setBackgroundResource(R.drawable.custom_button);
        b3.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean("dontshowagain", true);
                    editor.putInt("rated", 1);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });
        ll.addView(b3);

        dialog.setContentView(ll);
        dialog.show();
    }
}