package com.net;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.getmorelikes.pro.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.util.Config.log_tag;


public class JSONClientImage extends AsyncTask<String, Void, JSONObject> {
    AlertDialog progressDialog;
    GetJSONListener getJSONListener;
    Activity curContext;
    boolean result, blurryBackground;
    String loadingDialog;
    boolean showDialog;
    boolean showHart = false;
    long timeStart;

    public JSONClientImage(Activity context, GetJSONListener listener, boolean blurryBackground, String loadingDialog, boolean showHart, boolean showDialog) {
        this.getJSONListener = listener;
        this.curContext = context;
        this.blurryBackground = blurryBackground;
        this.loadingDialog = loadingDialog;
        this.showDialog = showDialog;
        this.showHart = showHart;
    }

    @Override
    public void onPreExecute() {
//		progressDialog = new ProgressDialog(curContext);
//		progressDialog.setMessage("Getting new photos please wait...");
//		progressDialog.setCancelable(false);
//		progressDialog.show();
        // if (showDialog)
        // handler.sendEmptyMessage(0);
    }

    @Override
    protected JSONObject doInBackground(String... urls) {
        try {
            Log.d(log_tag, "JSONClient2 doInBackground");
            return connect(urls[0]);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.d(log_tag, "JSONClient2 doInBackground exception:" + e);
            e.printStackTrace();
        }

        return null;
        // Log.d(log_tag, "slept");
        // try {
        // return connect(urls[0]);
        // Thread.sleep(3000);
        //
        //
        // } catch (Exception m) {
        // m.printStackTrace();
        // return null;
        // }
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        super.onPostExecute(json);
        Log.d(log_tag, "JSONClient2 complete: " + json);
//		progressDialog.dismiss();
        getJSONListener.onRemoteCallComplete(json, true);
        // handler.sendEmptyMessage(1);
        // Log.d(log_tag, "onPostExecute : " + json);
        // Log.d(log_tag, "onPostExecute getJSONListener: " +
        // getJSONListener);
        // if (getJSONListener != null)
        // if (json != null)
        // getJSONListener.onRemoteCallComplete(json, true);

    }

    protected void onCancelled() {
        // do something, inform user etc.
        // if (isCancelled()) {
        Log.d(log_tag, "JSONClient2 canceled");
        progressDialog.dismiss();
        getJSONListener.onRemoteCallComplete(null, false);

        // }
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            // Log.d(log_tag, "convertStreamToString IOException:" + e);
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                // Log.d(log_tag, "convertStreamToString finally IOException:"
                // +
                // e);
                e.printStackTrace();
            }
        }
        // Log.d(log_tag, "json sbtostring:" + sb.toString());

        return sb.toString();
    }

    public JSONObject connect(String url) {
        // HttpClient httpclient = new DefaultHttpClient();
        Log.d(log_tag, "JSONClient2 in connect");
        SharedPreferences sharedpreferences = curContext.getApplicationContext().getSharedPreferences("ig-user", 0);
        String timeout = sharedpreferences.getString("timeout", "15");
        HttpParams httpParameters = new BasicHttpParams();
        int timeoutConnection = Integer.parseInt(timeout) * 1000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = Integer.parseInt(timeout) * 1000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        HttpClient httpclient = new DefaultHttpClient(httpParameters);
        HttpGet httpget = new HttpGet(url);

        new M(url);

        // Execute the request
        HttpResponse response = null;
        Log.d(log_tag, "JSONClient2 vonnect url:" + url);
        try {
            Log.d(log_tag, "JSONClient2 in try:");
//			Timer timer = new Timer();
//			timer.schedule(new TaskKiller(this), 1000);

            response = httpclient.execute(httpget);

            // Examine the response status
            Log.i("Praeda", response.getStatusLine().toString());

            // Get hold of the response entity
            HttpEntity entity = response.getEntity();

            if (entity != null) {

                // A Simple JSON Response Read
                InputStream instream = entity.getContent();
                // Log.d(log_tag, "instreamtoStringlength:" +
                // instream.toString());
                // Log.d(log_tag, "instreamtoStringlength:" +
                // instream.toString().length());
                String result = convertStreamToString(instream);
                new M(" =================================================================================================================================");
                new M(" =================================================================================================================================");
                new M(" =================================================================================================================================");
                new M(" ========================================================= RESULT ================================================================");
                new M(" Response " + result);
                new M(" ========================================================== END ==================================================================");
                new M(" =================================================================================================================================");
//				Log.d("JSONClient " + url, result);
                Log.d(log_tag, "JSONClientimage response: url" + url);
                Log.d(log_tag, "JSONClientimage response: result:" + result);
                // A Simple JSONObject Creation
                JSONObject json;
                // A Simple JSONObject Creation
                if (result.contains("Content not found")) {
                    Log.d(log_tag, "JSONClientimage responsenotfoundif 1");
                    json = new JSONObject("{\"content\":\"not_found\"}");
                } else {
                    json = new JSONObject("{\"content\":\"found\"}");
                }

                // Closing the input stream will trigger connection release
                instream.close();
//				timer.cancel();
                return json;
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            Log.d(log_tag, "JSONClient2 ClientProtocolException: " + e);
            cancel(true);
            // this.cancel(true);
            e.printStackTrace();
        } catch (IOException e) {

            Log.d(log_tag, "JSONClient2 IOException: " + e);
            // TODO Auto-generated catch block
            e.printStackTrace();
            cancel(true);
        } catch (JSONException e) {

            Log.d(log_tag, "JSONClient2 JSONException: " + e);// TODO
            // Auto-generated
            // catch block
            e.printStackTrace();
            cancel(true);
        } catch (Exception m) {
            Log.d(log_tag, "JSONClient2 Exception: " + m);
            m.printStackTrace();
            cancel(true);
        }

        return null;
    }

    public interface GetJSONListener {
        public void onRemoteCallComplete(JSONObject jsonFromNet, boolean result);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (msg.what == 0) {
                if (curContext != null) {
                    progressDialog = new ProgressDialog(curContext);
                    progressDialog.setCancelable(true);
                    progressDialog.show();

                    if (!showHart)
                        progressDialog.setMessage(loadingDialog);

                    if (showHart) {
                        progressDialog.setContentView(R.layout.widget_loading_dialog);
                    }
                }

            }

            if (msg.what == 1) {
                if (progressDialog != null) {
                    if (progressDialog.isShowing()) {
                        try {
                            progressDialog.dismiss();
                            progressDialog = null;
                        } catch (Exception e) {
                            Log.d(log_tag, "dialog exception:" + e);
                        }
                    }
                    // progressDialog.dismiss();
                }
            }

        }
    };
}

