package com.liesinstagramwithads;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.getmorelikes.pro.R;
import com.google.android.gms.analytics.GoogleAnalytics;

import java.util.List;

@SuppressLint("NewApi")
public class ShareFragment extends Fragment {
    View rootView;
    Button share_button;
    TextView skip;
    String url, coinsNo, mediaId;
    int position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		Tracker app_tracker = ((LB) ShareFragment.this.getActivity().getApplication()).getTracker(LB.TrackerName.APP_TRACKER);
//		Tracker global_tracker = ((LB) this.getActivity().getApplication()).getTracker(LB.TrackerName.GLOBAL_TRACKER);
//		app_tracker.setScreenName("Share");
//		app_tracker.send(new HitBuilders.AppViewBuilder().build());
//		global_tracker.send(new HitBuilders.AppViewBuilder().build());
        rootView = inflater.inflate(R.layout.share_layout, container, false);
        share_button = (Button) rootView.findViewById(R.id.share_button);
        share_button.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                SharedPreferences sharedpreferences = ShareFragment.this.getActivity().getApplicationContext().getSharedPreferences("ig-user", 0);
                String URLtoShare = sharedpreferences.getString("URLtoShare", null);
                // intent.putExtra(android.content.Intent.EXTRA_TEXT,
                // URLtoShare);
                intent.putExtra(android.content.Intent.EXTRA_TEXT, URLtoShare);
                PackageManager pm = v.getContext().getPackageManager();
                List<ResolveInfo> activityList = pm.queryIntentActivities(intent, 0);
                // for (final ResolveInfo app : activityList) {
                // Log.d(log_tag,"app.activityInfo.name:"+app.activityInfo.name);
                // Log.d(log_tag,"app.activityInfo.processName:"+app.activityInfo.processName);
                // Log.d(log_tag,"app.activityInfo.packageName:"+app.activityInfo.packageName);
                // if
                // ((app.activityInfo.name).contains("com.facebook.composer")) {
                // // if
                // ((app.activityInfo.processName).contains("com.facebook.orca"))
                // {
                // final ActivityInfo activity = app.activityInfo;
                // final ComponentName name = new
                // ComponentName(activity.applicationInfo.packageName,
                // activity.name);
                // intent.addCategory(Intent.CATEGORY_LAUNCHER);
                // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                // |Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                // intent.setComponent(name);
                // //v.getContext().startActivity(intent);
                //
                // break;
                // }
                // }

                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.share_app)), 7865);

            }

        });
        skip = (TextView) rootView.findViewById(R.id.skip);
        skip.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoPay();
            }

        });
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        //Log.d(log_tag, "onstart likes analitics");
        // Get an Analytics tracker to report app starts & uncaught exceptions
        // etc.
        // GoogleAnalytics.getInstance(this).reportActivityStart(this);
        GoogleAnalytics.getInstance(ShareFragment.this.getActivity().getApplicationContext()).reportActivityStart(this.getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        // Stop the analytics tracking
        GoogleAnalytics.getInstance(ShareFragment.this.getActivity().getApplicationContext()).reportActivityStop(this.getActivity());
        // GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @SuppressLint("NewApi")
    public void gotoPay() {
        Intent intentShop = new Intent(ShareFragment.this.getActivity(), Shop.class);
        startActivity(intentShop);
//		Fragment fragment;
//		FragmentManager fragmentManager = getFragmentManager();
//		Bundle bundle = this.getArguments();
//		Log.d(log_tag, "bundle gotopay:" + bundle);
//		if (bundle != null) {
//			url = bundle.getString("url", "0");
//			mediaId = bundle.getString("id", "0");
//			coinsNo = bundle.getString("coins", "0");
//			position = bundle.getInt("position", 0);
//		}
//		if (position == 0)
//			fragment = new Shop();
//		else
//			fragment = new Pay();
//		fragment.setArguments(bundle);
//		fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.frame_container, fragment).commit();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //super.onActivityResult(requestCode, resultCode, data);
//		Log.d(log_tag, "onActivityResultrequestCode:" + requestCode);
//		Log.d(log_tag, "onActivityResultresultCode:" + resultCode);
//		Log.d(log_tag, "onActivityResultdata:" + data);

        if (requestCode == 7865) {
            getFragmentManager().popBackStack();
            //gotoPay();
        }
    }
}
