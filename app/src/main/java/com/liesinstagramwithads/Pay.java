package com.liesinstagramwithads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.datasets.ItemBatch;
import com.getmorelikes.pro.R;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.liesinstagramwithads.UrlEngine.GetCompletionListener;
import com.liesinstagramwithads.UrlEngine.GetSaveData;
import com.net.MImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.util.Config.log_tag;
//import com.google.android.gms.ads.AdView;
//import com.google.android.gms.ads.AdRequest;

public class Pay extends Fragment {
    MImageLoader loader;
    int target = 0;
    Activity activity;
    String mediaId, access_token = "0";
    String url, coinsNo, userPoints = "45";
    TextView mCoins, noOfLikes;
    Button onelike, tenlike, quarterlike, morelike;
    int noOfL;
    View view;
    private boolean isChangedStat = false;
    InterstitialAd interstitial;
    ArrayList<ItemBatch> itemes = new ArrayList<ItemBatch>();
    int index = 0;
    int noActions;
    int coins;
    int campaignLikes;
    LinearLayout goPro, linear_pay;
    SharedPreferences sharedpreferences;

    // AdView adView;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Log.d(log_tag, "in pay");
        getActivity().getActionBar().setTitle(R.string.get_likes);
        sharedpreferences = Pay.this.getActivity().getApplicationContext().getSharedPreferences("ig-user", 0);
        String PModal = sharedpreferences.getString("PModal", null);
        String adMobId = sharedpreferences.getString("adMobId", null);
        noActions = sharedpreferences.getInt("noActions", 0);

        // super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		Tracker app_tracker = ((LB) Pay.this.getActivity().getApplication()).getTracker(LB.TrackerName.APP_TRACKER);
//		Tracker global_tracker = ((LB) this.getActivity().getApplication()).getTracker(LB.TrackerName.GLOBAL_TRACKER);
//		app_tracker.setScreenName("Get Likes");
//		app_tracker.send(new HitBuilders.AppViewBuilder().build());
//		global_tracker.send(new HitBuilders.AppViewBuilder().build());

        view = inflater.inflate(R.layout.activity_get_likes, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            url = bundle.getString("url", "0");
            mediaId = bundle.getString("id", "0");
            coinsNo = bundle.getString("coins", "0");

        }
        Log.d(log_tag, "bundle pay:" + bundle);
        coins = Integer.parseInt(coinsNo);
        ImageView image = (ImageView) view.findViewById(R.id.image1);
        // onelike = (Button) view.findViewById(R.id.oneLikeButton);
        noOfLikes = (TextView) view.findViewById(R.id.noOfLikes);
        onelike = (Button) view.findViewById(R.id.oneLikeButton);
        tenlike = (Button) view.findViewById(R.id.tenLikeButton);
        quarterlike = (Button) view.findViewById(R.id.quarterLikeButton);
        morelike = (Button) view.findViewById(R.id.moreLikeButton);
        loader = new MImageLoader(this.getActivity());
        campaignLikes = sharedpreferences.getInt(mediaId + "_campaignLikes", 0);
        // Editor edit=sharedpreferences.edit();
        // edit.putInt(mediaId+"_campaignLikes",campaignLikes);
        // noOfLikes.setText(activity.getResources().getString(R.string.campaign_start)
        // + " : +"+campaignLikes+" Like");
        // noOfLikes.setText("hh"+campaignLikes);
        image.setTag(url);
        loader.DisplayImage(url, this.getActivity(), image);
        activity = Pay.this.getActivity();

        // UrlEngine.getNoOfLikes(activity, mediaId, LB.getInstagramUser().id,
        // new GetData() {
        // @Override
        // public void OnCompletion(JSONObject json) {
        // JSONArray array = null;
        // // Log.d(log_tag, "getNoOfLikes json:" + json);
        // try {
        // // Log.d(log_tag, "getNoOfLikes count:" +
        // // json.getString("count"));
        // noOfLikes.setText(json.getString("count") + " Likes");
        // } catch (JSONException e) {
        // // TODO Auto-generated catch block
        // Log.d(log_tag, "getNoOfLikes JSONException:" + e);
        // e.printStackTrace();
        // }
        // }
        // });

        ArrayList<String> data = new ArrayList<String>();
        data.add("testing");
        data.add("testing");
        data.add("testing");
        data.add("testing");
        // data.add("testing");
        // data.add("testing");

        Log.d(log_tag, "data:" + data);
        // PayAdapter adapter = new PayAdapter(Pay.this.getActivity(), data,
        // mediaId, url, mCoins);
        // ListView list = (ListView) view.findViewById(R.id.listShop);
        // list.setAdapter(adapter);
        onelike.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                doButtonClick(5);
            }
        });
        tenlike.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                doButtonClick(10);
            }
        });
        quarterlike.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                doButtonClick(25);
            }
        });
        morelike.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                doButtonClick(0);
            }
        });
        return view;
    }

    // @Override
    // public void onListItemClick(ListView l, View v, int position, long id) {
    // String item = (String) getListAdapter().getItem(position);
    // Log.d(log_tag,"lisclickitem:"+item)
    // // Toast.makeText(this, item + " selected", Toast.LENGTH_LONG).show();
    // }
    public void ShopDialog(final Activity act) {
        Log.d(log_tag, "in ShopDialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(act);

        builder.setTitle("Get more likes");
        builder.setMessage("Visit our shop to get more coins and boost your likes");

        builder.setPositiveButton("Get likes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                // Intent intent = new Intent(act, Shop.class);
                // act.startActivity(intent);
                Intent intentShop = new Intent(Pay.this.getActivity(), Shop.class);
                Pay.this.getActivity().startActivity(intentShop);
                // Fragment fragment = null;
                // fragment = new Shop();
                // FragmentManager fragmentManager = act.getFragmentManager();
                // //
                // fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.frame_container,
                // // fragment).commit();
                //
                // if (fragmentManager != null) {
                // FragmentTransaction ft = fragmentManager.beginTransaction();
                //
                // ft.addToBackStack(null);
                //
                // ft.replace(R.id.frame_container, fragment);
                // ft.commit();
                // //
                // Pay.this.getActivity().getFragmentManager().beginTransaction().replace(R.id.frame_container,
                // }
                dialog.dismiss();
            }

        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    protected void doButtonClick(int i) {
        final int target = i;
        int coins = target * 2;
        android.content.SharedPreferences.Editor editor = sharedpreferences.edit();
        if (i == 0) {
            Intent intentShop = new Intent(Pay.this.getActivity(), Shop.class);
            startActivity(intentShop);
            // Fragment nextFrag = new Shop();
            // FragmentManager fm = activity.getFragmentManager();
            //
            // if (fm != null) {
            // FragmentTransaction ft = fm.beginTransaction();
            //
            // ft.addToBackStack(null);
            // ft.replace(R.id.frame_container, nextFrag);
            // ft.commit();
            // Pay.this.getActivity().getFragmentManager().beginTransaction().replace(R.id.frame_container,
            // nextFrag).addToBackStack(null).commit();
            // }
        }
        if (i != 0) {
            // TODO Auto-generated method stub
            if (Integer.parseInt(LB.getInstagramUser().getUserPoints()) < coins) {
                ShopDialog(activity);

            } else {
                if (android.os.Build.VERSION.SDK_INT > 9) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                }
                UrlEngine.starCampaign(activity, mediaId, url, target + "", coins, LB.getInstagramUser().id, new GetSaveData() {
                    @Override
                    public void OnCompletion(JSONObject json) {
                        UrlEngine.autolike(activity, mediaId, url, target);
                        try {
                            if (json != null) {
                                int campaignId = json.getInt("campaign_id");
                                UrlEngine.addUltraFastLikes(activity, LB.getInstagramUser().id, campaignId + "", target + "", new GetCompletionListener() {
                                    @Override
                                    public void OnCompletion(ArrayList<ItemBatch> data) {
                                        // activity.finish();
                                    }
                                });
                            } else {
                                AlertDialog ad = new AlertDialog.Builder(getActivity())

                                        // Set Dialog Title
                                        .setTitle("Server error")
                                        // Set Dialog Message
                                        // .setMessage("Click ok to retry")

                                        // Positive button
                                        // .setPositiveButton("OK", new
                                        // DialogInterface.OnClickListener() {
                                        // public void onClick(DialogInterface
                                        // dialog, int which) {
                                        // // Do something else
                                        // Log.d(log_tag, "clicked ok");
                                        // loadNewItems();
                                        // }
                                        // })

                                        // Negative Button
                                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                Log.d(log_tag, "clicked cancel");
                                                // Do something else
                                            }
                                        }).create();
                                ad.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // android.content.SharedPreferences.Editor editor =
                        // sharedpreferences.edit();
                        // editor.putString("noLikes", "nolikes");
                        // editor.commit();
                        activity.registerForContextMenu(view);
                        activity.invalidateOptionsMenu();
                    }

                    @Override
                    public void comple(String json) {
                        UrlEngine.autolike(activity, mediaId, url, target);
                        JSONObject jsoner;
                        try {
                            jsoner = new JSONObject(json);
                            int campaignId = jsoner.getInt("campaign_id");
                            UrlEngine.addUltraFastLikes(activity, LB.getInstagramUser().id, campaignId + "", target + "", new GetCompletionListener() {
                                @Override
                                public void OnCompletion(ArrayList<ItemBatch> data) {
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
        String likesTxt = noOfLikes.getText().toString();
        if (likesTxt.length() > 0) {
            // String noLikes = likesTxt.substring(0,
            // likesTxt.indexOf("Like"));
            String noLikes = likesTxt.substring(likesTxt.indexOf("+") + 1, likesTxt.indexOf("Like"));
            int likes = Integer.parseInt(noLikes.trim());
            int setcampaignLikes = likes + i;
            editor.putInt(mediaId + "_campaignLikes", setcampaignLikes);
            Log.d(log_tag, "campaignLikes2: " + mediaId + "_campaignLikes :" + setcampaignLikes);

            editor.commit();
            noOfLikes.setText(activity.getResources().getString(R.string.campaign_start) + " : +" + setcampaignLikes + " Likes");
        } else {
            noOfLikes.setText(activity.getResources().getString(R.string.campaign_start) + " : +" + i + " Likes");
        }
        Log.d(log_tag, "shared prefs in pay:" + sharedpreferences);
        SharedPreferences sharedpreferences = activity.getSharedPreferences("ig-user", 0);
        String PModal = sharedpreferences.getString("PModal", "15");
        String PShare = sharedpreferences.getString("PShare", "50");
        String PRate = sharedpreferences.getString("PRate", "50");
        Log.d(log_tag, "shared prefs in pay PModal:" + PModal);
        Log.d(log_tag, "shared prefs in pay PShare:" + PShare);
        Log.d(log_tag, "shared prefs in pay PRate:" + PRate);
        int rated = sharedpreferences.getInt("rated", 80);
        int probModal = Integer.parseInt(PModal);
        int probShare = Integer.parseInt(PShare);
        int probRate = Integer.parseInt(PRate);
        Log.d(log_tag, "shared prefs in pay PRate:" + PRate);
        Log.d(log_tag, "shared prefs in pay rated:" + rated);
        // int probRate = Integer.parseInt(PRate);
        int checkModal = (int) (Math.random() * 100);
        // checkModal=13;
        Fragment nextFrag = new ShareFragment();
        Log.d(log_tag, "probShare: " + probShare + " checkModal:" + checkModal + " probmodal:" + probModal + " probRate:" + probRate);
        /*
		 * if (checkModal < probModal) { int check2 = (int) (Math.random() *
		 * 100); // check2=60; if (check2 < probShare) { Bundle bundle = new
		 * Bundle(); bundle.putString("url", url); bundle.putString("id",
		 * mediaId); bundle.putInt("position", i); // bundle.putString("coins",
		 * mCoins); Log.d(log_tag, "bundle: " + bundle);
		 * nextFrag.setArguments(bundle); FragmentManager fm =
		 * activity.getFragmentManager(); if (fm != null) { FragmentTransaction
		 * ft = fm.beginTransaction();
		 * 
		 * ft.addToBackStack(null);
		 * 
		 * ft.replace(R.id.frame_container, nextFrag); ft.commit(); } } else if
		 * (rated != 1) { // if (checkModal < probRate) {
		 * AppRater.app_launched(activity); // } } }
		 */

    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.d(log_tag, " pay onCreateOptionsMenu" + userPoints);
        // Do something that differs the Activity's menu here
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        Log.d(log_tag, " pay onPrepareOptionsMenu ");
        menu.clear();

        if (isChangedStat) {

            menu.add(0, 0, 0, "True");

        } else {

            menu.add(0, 0, 0, "False");

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(log_tag, "onstart pay analitics");
        // Get an Analytics tracker to report app starts & uncaught exceptions
        // etc.
        // GoogleAnalytics.getInstance(this).reportActivityStart(this);
        GoogleAnalytics.getInstance(Pay.this.getActivity().getApplicationContext()).reportActivityStart(this.getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        interstitial = null;
        // Stop the analytics tracking
        // GoogleAnalytics.getInstance(this).reportActivityStop(this);
        GoogleAnalytics.getInstance(Pay.this.getActivity().getApplicationContext()).reportActivityStop(this.getActivity());
    }

}
