package com.net;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.liesinstagramwithads.UrlEngine.TaskComplete;

public class RestNetCaller extends AsyncTask<String, Void, String> {
    private TaskComplete listener;
    Context curContext;
    AlertDialog progressDialog;
    private RestClient mRestClient;
    String message;

    public RestNetCaller(Activity context, String url, TaskComplete listenerr, String message) {
        this.curContext = context;
        listener = listenerr;
        mRestClient = new RestClient(url);
        this.message = message;
    }

    @Override
    public void onPreExecute() {
        Log.d("log_tag", "restnetcaller onPreExecute");
        mRestClient.setContext(curContext);
        if (!message.equals("")) {
            progressDialog = new ProgressDialog(curContext);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    protected String doInBackground(String... urls) {
        Log.d("log_tag", "restnetcaller doInBackground:" + urls);
        // Log.d("log_tag","restnetcaller doInBackground:"+urls[0]);
        try {
            mRestClient.execute(3);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mRestClient.getResponse();
    }

    @Override
    protected void onPostExecute(String json) {
        Log.d("log_tag", "restnetcaller onPostExecute:" + json);
        if (json == null) {
            Log.d("log_tag", "restnetcaller onPostExecute null:" + json);
        }

        if (progressDialog != null)
            progressDialog.dismiss();
        listener.complete(json, this);
    }

    public RestClient getRestClient() {
        return mRestClient;
    }
}