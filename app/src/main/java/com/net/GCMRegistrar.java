// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.net;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

// Referenced classes of package com.google.android.gcm:
//            GCMBroadcastReceiver

public final class GCMRegistrar {

    private static final String BACKOFF_MS = "backoff_ms";
    private static final int DEFAULT_BACKOFF_MS = 3000;
    public static final long DEFAULT_ON_SERVER_LIFESPAN_MS = 0x240c8400L;
    private static final String GSF_PACKAGE = "com.google.android.gsf";
    private static final String PREFERENCES = "com.google.android.gcm";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String PROPERTY_ON_SERVER = "onServer";
    private static final String PROPERTY_ON_SERVER_EXPIRATION_TIME = "onServerExpirationTime";
    private static final String PROPERTY_ON_SERVER_LIFESPAN = "onServerLifeSpan";
    private static final String PROPERTY_REG_ID = "regId";
    private static final String TAG = "GCMRegistrar";
    //    private static GCMBroadcastReceiver sRetryReceiver;
    private static String sRetryReceiverClassName;

    private GCMRegistrar() {
        throw new UnsupportedOperationException();
    }

    public static void checkDevice(Context context) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i < 8) {
            throw new UnsupportedOperationException((new StringBuilder()).append("Device must be at least API Level 8 (instead of ").append(i).append(")").toString());
        }
        PackageManager packagemanager = context.getPackageManager();
        try {
            packagemanager.getPackageInfo("com.google.android.gsf", 0);
            return;
        } catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception) {
            throw new UnsupportedOperationException("Device does not have package com.google.android.gsf");
        }
    }

    public static void checkManifest(Context context) {
        PackageManager packagemanager = context.getPackageManager();
        String s = context.getPackageName();
        String s1 = (new StringBuilder()).append(s).append(".permission.C2D_MESSAGE").toString();
        PackageInfo packageinfo;
        ActivityInfo aactivityinfo[];
        try {
            packagemanager.getPermissionInfo(s1, 4096);
        } catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception) {
            throw new IllegalStateException((new StringBuilder()).append("Application does not define permission ").append(s1).toString());
        }
        try {
            packageinfo = packagemanager.getPackageInfo(s, 2);
        } catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception1) {
            throw new IllegalStateException((new StringBuilder()).append("Could not get receivers for package ").append(s).toString());
        }
        aactivityinfo = packageinfo.receivers;
        if (aactivityinfo == null || aactivityinfo.length == 0) {
            throw new IllegalStateException((new StringBuilder()).append("No receiver for package ").append(s).toString());
        }
        if (Log.isLoggable("GCMRegistrar", 2)) {
            Log.v("GCMRegistrar", (new StringBuilder()).append("number of receivers for ").append(s).append(": ").append(aactivityinfo.length).toString());
        }
        HashSet hashset = new HashSet();
        int i = aactivityinfo.length;
        for (int j = 0; j < i; j++) {
            ActivityInfo activityinfo = aactivityinfo[j];
            if ("com.google.android.c2dm.permission.SEND".equals(activityinfo.permission)) {
                hashset.add(activityinfo.name);
            }
        }

        if (hashset.isEmpty()) {
            throw new IllegalStateException("No receiver allowed to receive com.google.android.c2dm.permission.SEND");
        } else {
            checkReceiver(context, hashset, "com.google.android.c2dm.intent.REGISTRATION");
            checkReceiver(context, hashset, "com.google.android.c2dm.intent.RECEIVE");
            return;
        }
    }

    private static void checkReceiver(Context context, Set set, String s) {
        PackageManager packagemanager = context.getPackageManager();
        String s1 = context.getPackageName();
        Intent intent = new Intent(s);
        intent.setPackage(s1);
        List list = packagemanager.queryBroadcastReceivers(intent, 32);
        if (list.isEmpty()) {
            throw new IllegalStateException((new StringBuilder()).append("No receivers for action ").append(s).toString());
        }
        if (Log.isLoggable("GCMRegistrar", 2)) {
            Log.v("GCMRegistrar", (new StringBuilder()).append("Found ").append(list.size()).append(" receivers for action ").append(s).toString());
        }
        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
            String s2 = ((ResolveInfo) iterator.next()).activityInfo.name;
            if (!set.contains(s2)) {
                throw new IllegalStateException((new StringBuilder()).append("Receiver ").append(s2).append(" is not set with permission ").append("com.google.android.c2dm.permission.SEND").toString());
            }
        }

    }

    static String clearRegistrationId(Context context) {
        return setRegistrationId(context, "");
    }

    private static int getAppVersion(Context context) {
        int i;
        try {
            i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception) {
            throw new RuntimeException((new StringBuilder()).append("Coult not get package name: ").append(namenotfoundexception).toString());
        }
        return i;
    }

    static int getBackoff(Context context) {
        return getGCMPreferences(context).getInt("backoff_ms", 3000);
    }


    private static SharedPreferences getGCMPreferences(Context context) {
        return context.getSharedPreferences("com.google.android.gcm", 0);
    }

    public static long getRegisterOnServerLifespan(Context context) {
        return getGCMPreferences(context).getLong("onServerLifeSpan", 0x240c8400L);
    }

    public static String getRegistrationId(Context context) {
        SharedPreferences sharedpreferences = getGCMPreferences(context);
        String s = sharedpreferences.getString("regId", "");
        int i = sharedpreferences.getInt("appVersion", 0x80000000);
        int j = getAppVersion(context);
        if (i != 0x80000000 && i != j) {
            Log.v("GCMRegistrar", (new StringBuilder()).append("App version changed from ").append(i).append(" to ").append(j).append("; resetting registration id").toString());
            clearRegistrationId(context);
            s = "";
        }
        return s;
    }


    static void internalUnregister(Context context) {
        Log.v("GCMRegistrar", (new StringBuilder()).append("Unregistering app ").append(context.getPackageName()).toString());
        Intent intent = new Intent("com.google.android.c2dm.intent.UNREGISTER");
        intent.setPackage("com.google.android.gsf");
        intent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0));
        context.startService(intent);
    }

    public static boolean isRegistered(Context context) {
        return getRegistrationId(context).length() > 0;
    }

    public static boolean isRegisteredOnServer(Context context) {
        SharedPreferences sharedpreferences = getGCMPreferences(context);
        boolean flag = sharedpreferences.getBoolean("onServer", false);
        Log.v("GCMRegistrar", (new StringBuilder()).append("Is registered on server: ").append(flag).toString());
        if (flag) {
            long l = sharedpreferences.getLong("onServerExpirationTime", -1L);
            if (System.currentTimeMillis() > l) {
                Log.v("GCMRegistrar", (new StringBuilder()).append("flag expired on: ").append(new Timestamp(l)).toString());
                flag = false;
            }
        }
        return flag;
    }


    static void resetBackoff(Context context) {
        Log.d("GCMRegistrar", (new StringBuilder()).append("resetting backoff for ").append(context.getPackageName()).toString());
        setBackoff(context, 3000);
    }

    static void setBackoff(Context context, int i) {
        android.content.SharedPreferences.Editor editor = getGCMPreferences(context).edit();
        editor.putInt("backoff_ms", i);
        editor.commit();
    }

    public static void setRegisterOnServerLifespan(Context context, long l) {
        android.content.SharedPreferences.Editor editor = getGCMPreferences(context).edit();
        editor.putLong("onServerLifeSpan", l);
        editor.commit();
    }

    public static void setRegisteredOnServer(Context context, boolean flag) {
        android.content.SharedPreferences.Editor editor = getGCMPreferences(context).edit();
        editor.putBoolean("onServer", flag);
        long l = getRegisterOnServerLifespan(context) + System.currentTimeMillis();
        Log.v("GCMRegistrar", (new StringBuilder()).append("Setting registeredOnServer status as ").append(flag).append(" until ").append(new Timestamp(l)).toString());
        editor.putLong("onServerExpirationTime", l);
        editor.commit();
    }

    static String setRegistrationId(Context context, String s) {
        SharedPreferences sharedpreferences = getGCMPreferences(context);
        String s1 = sharedpreferences.getString("regId", "");
        int i = getAppVersion(context);
        Log.v("GCMRegistrar", (new StringBuilder()).append("Saving regId on app version ").append(i).toString());
        android.content.SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("regId", s);
        editor.putInt("appVersion", i);
        editor.commit();
        return s1;
    }


    static void setRetryReceiverClassName(String s) {
        Log.v("GCMRegistrar", (new StringBuilder()).append("Setting the name of retry receiver class to ").append(s).toString());
        sRetryReceiverClassName = s;
    }

    public static void unregister(Context context) {
        resetBackoff(context);
        internalUnregister(context);
    }
}
