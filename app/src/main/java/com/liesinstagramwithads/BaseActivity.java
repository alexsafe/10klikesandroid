package com.liesinstagramwithads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.adapter.NavDrawerListAdapter;
import com.billing.v3.IabHelper;
import com.datasets.ItemBatch;
import com.datasets.NavDrawerItem;
import com.getmorelikes.pro.R;
import com.liesinstagramwithads.UrlEngine.GetCompletionListener;
import com.liesinstagramwithads.UrlEngine.GetSaveData;
import com.net.InstagramApp;
import com.util.ConnectionDetector;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.util.Config.log_tag;

@SuppressLint("NewApi")
public abstract class BaseActivity extends Activity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;
    ConnectionDetector cd;
    // used to store app title
    private CharSequence mTitle;
    IabHelper mHelper;
    // slide menu items
    private String[] navMenuTitles;
    // private TypedArray navMenuIcons;
    ArrayList<ItemBatch> itemes = new ArrayList<ItemBatch>();
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    int index = 0;
    InstagramApp mApp;
    private boolean doubleBackToExitPressedOnce;
    FrameLayout actContent;

    private void loadCoins1(final Menu menu) {
        // menu.findItem(R.id.noOfCoins).setTitle(LB.getInstagramUser().getUserPoints()
        // + "");
        menu.findItem(R.id.noOfCoins).setTitle("400");
    }

    @Override
    public void setContentView(int layoutResID) {
        Log.d(log_tag, "baseactiv setContentView");
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        // navMenuIcons =
        // getResources().obtainTypedArray(R.array.nav_drawer_icons);

        // mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.base_activ_layout, null);

        actContent = (FrameLayout) mDrawerLayout.findViewById(R.id.frame_container_base);
        getLayoutInflater().inflate(layoutResID, actContent, true);
        super.setContentView(mDrawerLayout);

        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        // adding nav drawer items to array
        // Home
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3]));

        // Recycle the typed array
        // navMenuIcons.recycle();

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(adapter);

        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.drawer2, // nav
                // menu
                // toggle
                // icon
                R.string.app_name, // nav drawer open - description for
                // accessibility
                R.string.app_name // nav drawer close - description for
                // accessibility
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                // getActionBar().setTitle(String.valueOf(mTitle).toUpperCase(Locale.getDefault()));
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                // getActionBar().setTitle(String.valueOf(mDrawerTitle).toUpperCase(Locale.getDefault()));
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Log.d(log_tag, "baseactivity oncreate");
        // Tracker t = ((LB)
        // this.getApplication()).getTracker(TrackerName.APP_TRACKER);
        // t.setScreenName("Main Activity");
        // t.send(new HitBuilders.AppViewBuilder().build());

        // Log.d(log_tag, "on create main 1");
        checkConn();
        // getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.base_activ_layout);

        // getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bg));

        UrlEngine.getAppData(BaseActivity.this);
        mTitle = mDrawerTitle = getTitle();
        SharedPreferences sharedpreferences = this.getApplicationContext().getSharedPreferences("ig-user", 0);
        String access_token = sharedpreferences.getString("access_token", null);
        // android.content.SharedPreferences.Editor editor =
        // sharedpreferences.edit();
        // editor.putInt("rated", 0);
        // editor.commit();
        String NActions = sharedpreferences.getString("NActions", null);
        int rated = sharedpreferences.getInt("rated", 0);

        // AppRater.app_launched(this);
        // Log.d(log_tag, "onCreate NActions:" + NActions);
        // Log.d(log_tag, "onCreate access_token:" + access_token);
        // load slide menu items

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            // displayView(0);
        }
        // loadCoins(getActionBar().);

    }

    public void retry(View v) {
        Log.d(log_tag, "retry clicked");
    }

    // @Override
    // public void onBackPressed() {
    // final Pay fragment = (Pay)
    // getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);
    //
    // if (fragment.allowBackPressed()) { // and then you define a method
    // // allowBackPressed with the logic
    // // to allow back pressed or not
    // super.onBackPressed();
    // }
    // }

    public void checkConn() {
        Log.d(log_tag, "check internet connection");
        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Internet Connection Error");
            alertDialogBuilder.setMessage("Please connect to working Internet connection").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    BaseActivity.this.finish();
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return;
        }
    }

    private void loadCoins(final Menu menu) {
        Log.d(log_tag, "invalidate load coins");
        UrlEngine.getItemBatch(BaseActivity.this, LB.getInstagramUser().id, new GetCompletionListener() {
            @Override
            public void OnCompletion(ArrayList<ItemBatch> data) {
                Log.d(log_tag, "invalidate load coins completion");
                if (data == null) {

                    // final AlertDialog ad = new
                    // AlertDialog.Builder(BaseActivity.this)
                    //
                    // // Set Dialog Title
                    // .setTitle("Server timeout")
                    // // Set Dialog Message
                    // .setMessage("Click ok to retry loading correct number of coins")
                    //
                    // // Positive button
                    // .setPositiveButton("OK", new
                    // DialogInterface.OnClickListener() {
                    // public void onClick(DialogInterface dialog, int which) {
                    // // Do something else
                    // Log.d(log_tag, "clicked ok");
                    // loadCoins(menu);
                    // }
                    // })
                    //
                    // // Negative Button
                    // .setNegativeButton("Cancel", new
                    // DialogInterface.OnClickListener() {
                    // public void onClick(DialogInterface dialog, int which) {
                    // Log.d(log_tag, "clicked cancel");
                    // // Do something else
                    //
                    // }
                    // }).create();
                    // ad.show();
                    menu.findItem(R.id.noOfCoins).setTitle(LB.getInstagramUser().getUserPoints() + "");
                } else {
                    itemes = new ArrayList<ItemBatch>();
                    itemes.addAll(data);
                    Log.d(log_tag, "invalidate items in complete: " + itemes.size());
                    Log.d(log_tag, "invalidate items in complete: " + itemes);
                    if (itemes.size() > 0)
                        if (itemes.get(index).campaign_id != null)
                            UrlEngine.getStats(BaseActivity.this, LB.getInstagramUser().id, itemes.get(index).campaign_id, "0", new GetSaveData() {

                                @Override
                                public void OnCompletion(JSONObject json) {
                                    Log.d(log_tag, " invalidate on complete:" + json);

                                    // coins.setText(LB.getInstagramUser().getUserPoints()
                                    // +
                                    // "");
                                    Log.d(log_tag, "invalidate load coins:" + LB.getInstagramUser().getUserPoints());
                                    menu.findItem(R.id.noOfCoins).setTitle(LB.getInstagramUser().getUserPoints() + "");
                                    // menu.findItem(R.id.noOfCoins).setTitle("300");
                                    // coins.setText("10000");
                                }

                                @Override
                                public void comple(String json) {
                                    Log.d(log_tag, "on comple");
                                    // coins.setText(LB.getInstagramUser().getUserPoints()
                                    // +
                                    // "");
                                }
                            }, true);
                }
            }
        });
    }

    private static long back_pressed;

    @Override
    public void onBackPressed() {
        Log.d(log_tag, "onBackPressed");
        Log.d(log_tag, "onBackPressed getFragmentManager().getBackStackEntryCount():" + getFragmentManager().getBackStackEntryCount());

        if (getFragmentManager().getBackStackEntryCount() <= 1) {

            this.finish();
        } else {

            // super.onBackPressed();
            getFragmentManager().popBackStack();
        }
        // if (back_pressed + 2000 > System.currentTimeMillis()) {
        // // super.onBackPressed();
        // this.finish();
        // } else {
        // Toast.makeText(getBaseContext(), "Press once again to exit!",
        // Toast.LENGTH_SHORT).show();
        // back_pressed = System.currentTimeMillis();
        // }
    }

    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(log_tag, " main onCreateOptionsMenu:" + LB.getInstagramUser().getUserPoints());
        Log.d(log_tag, " main onCreateOptionsMenu:" + LB.getInstagramUser().points);

        getMenuInflater().inflate(R.menu.main, menu);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String likes = sharedPreferences.getString("likes", "50");
        String title = sharedPreferences.getString("title", "Get More Likes PRO");
        // Log.d(log_tag, "likes prefs: " + likes);
        menu.getItem(0).setTitle(LB.getInstagramUser().getUserPoints() + "");
        // menu.add(3, 3, Menu.NONE,
        // getApplicationContext().getString(R.string.hello_world));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                // Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                // intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
                // catch event that there's no activity to handle intent

                // startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        menu.findItem(R.id.noOfCoins).setVisible(!drawerOpen);
        Log.d(log_tag, "invalidate in meniu");
//		loadCoins(menu);
        return super.onPrepareOptionsMenu(menu);
    }

    // @Override
    // protected void onActivityResult(int requestCode, int resultCode, Intent
    // data) {
    // Log.d(log_tag,"main onActivityResult");
    // // if (mHelper == null)
    // // return;
    //
    // // Pass on the activity result to the helper for handling
    // if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
    // // not handled, so handle it ourselves (here's where you'd
    // // perform any handling of activity results not related to in-app
    // // billing...
    // super.onActivityResult(requestCode, resultCode, data);
    // }
    // }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;

        Log.d(log_tag, "display poz:" + position);
        switch (position) {
            case 0:
                // fragment = new Rate();
                Intent intent2 = new Intent(BaseActivity.this, MainActivity.class);
                intent2.putExtra("screen", "Rate");
                startActivity(intent2);
                break;
            case 1:

                Intent intent1 = new Intent(BaseActivity.this, MainActivity.class);
                intent1.putExtra("screen", "selectPhotos");
                startActivity(intent1);
                // fragment = new SelectPhoto();
                break;
            case 2:
                // Intent intent = new Intent(this, Shop.class);
                //
                // startActivityForResult(intent, 111);
                Intent intentShop = new Intent(BaseActivity.this, Shop.class);
                startActivity(intentShop);
                break;
            case 3:
                fragment = new LogOut();
                break;
            default:
                break;
        }

        if (fragment != null) {
            // Log.d(log_tag, "fragment not null");

            // Display a simple Toast to demonstrate that the click event is
            // working. Notice that Fragments have a
            // getString() method just like an Activity, so that you can quickly
            // access your localized Strings.
            // Toast.makeText(activity, getString(R.string.toast_item_click) +
            // locationModel.address, Toast.LENGTH_SHORT).show();

            FragmentManager fragmentManager = getFragmentManager();
            // fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.frame_container,
            // fragment).commit();

            if (fragmentManager != null) {
                FragmentTransaction ft = fragmentManager.beginTransaction();

                ft.addToBackStack(null);

                ft.replace(R.id.frame_container_base, fragment);
                ft.commit();
                // Pay.this.getActivity().getFragmentManager().beginTransaction().replace(R.id.frame_container,
            }
            // fragmentManager.beginTransaction().add(R.id.gridLikes,
            // fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("BaseActivity", "Error in creating fragment");
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
        // getActionBar().setTitle(String.valueOf(mTitle).toUpperCase(Locale.getDefault()));
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        checkConn();
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

}