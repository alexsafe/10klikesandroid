package com.util;

public class Config {
    public static final String log_tag = "alex_tag";
    public static final String analitics_id = "UA-52964490-41";
    public static final String admob_id = "ca-app-pub-3307731116933970/2215828846";
    public static final String intersitial_id = "ca-app-pub-3307731116933970/3692562041";
    //	public static final String encodedPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuSS1pX98QO0K+rzPLfa/Q2zbVDZNGhenZD/c+/sCLSO5LeHIMmas6FejoZL8y6kOfhtcYcRuCYpgthGoTpYGcZB+T+XYkvmLnm8UTNfvsY66NpQ3CW2WN4SaX+Hph6hhzUjL7T5rVrWGlARbPELIHOmowaW32MEGf2HVzATnn5QrW6aI9BozI6Ib02oOw/rHNF2ef13Ocjm6Fy4K26eyetu0seeaG6kly6KBWBrrdE5lzt8ORMqZ4YwbdCaEombapROYo81bVnpELT7QpPutFkMeg4tKV2IMB/2E9IDmM171INYKUdlXWAJEd5jzj0Fs0qtOkJFoXbfrPl4wth7P2QIDAQAB";
    // ads pro public static final String encodedPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgmTYU932O0G7VLNFkFekq8F5HsVsPS2h6uK6boxFcTuq3rwePb/qrmWR2KwprPbR7jEcIhB+qrfkEk9UGhB6yyOISZBYZdarAvXAxHMQzst11N9pf6PEohz8HoZVRrKbZe2elbQCaj2xLkvV5v7BdNL1rwvpkUhp6a0R8t0pQ8iNSZHkBYP+XJkijsoIfnw6hJE1VcvB1zQwAzpe1AgE79bhOiumeAO0PhkLi5SyitSRqAMuJnbadB4ZlQJ1qbxaUqrDzDZETpv2DiG6w6OP4Xxb0HlWZhka2alSqIFHitXhuL1+R69aycDwqnK/jFgXFwt3WPZRKG4pgEx2tCuyewIDAQAB";
    //public static final String encodedPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsvSbsZ2BIN/5NkPZ1/rVwIQpBMeTOrEYVEs9RnmNCGVhvzEZlcHL6jwppHU1Cbkn4Dd5Kr1fd87ZFXfCcJJS1V+5Jaj5OUNUhwuEVt+9obh6Z3iU8W+U8tWwOxjUnzaGwmSrM1NK0qxf8avgOZ1Npw688zfxwdvi0AYX5noVsV/NxIEfc0ydSlvFNR4KiEx8kxWf3nF8i1Bsmx704OUaxMhB2esPPsctZ7W/k5wxXjIMs3h28HR5mIjssfh8urK2pZkl0YqHOa47bhBXj/Z2eqbn/Lr++ls+xszgIxmqyB8B3ICDlz666K5E72S8XBPOK+NzmgCeBzVGU57ouvAfHwIDAQAB";
    public static final String encodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvUOuHvtS3d24xqEYMEQ0ibpLeuY27UktkYTBYRiyO1TDre/GAGdPqHf8wnkD/C6yEipf48Gu8jI3bmVwjYndfDxiU9+fKxAiGx0pSYYn4SreX0T6rhlb4r+E/A/SEYukGZAqaGaNEv97VdCG2ZXYfribh8nsy0t79GpcTnK8CZJsw1lgEH79A1ihhp3xuAOSW9u4wz7Luni827AFqxksl5rRF+ufHb0awWELAygvK6vTcPeTirwWbbNujsp28OwZ0brYt+T3gbIvgMzSOL0U6ST4zNmH93wAxTOL4M/1lbjQyW92a2OwPP+JaQUC+PZWyS7z7L62nLA4rEnmQe7m8wIDAQAB";
    public static final String baseUrl = "http://dev.somasocial.com/10000likespro/";

    public static final String CLIENT_ID = "0508a0f6eb2e4eecae660e52a659a884";
    public static final String CLIENT_SECRET = "6fdca25b904c406e8353795cb3a02cd8";
    public static final String CALLBACK_URL = "http://soma.greenlab.ro/10000likespro/callback_uri.php";

    private static final String AUTH_URL = "https://api.instagram.com/oauth/authorize/";
    private static final String TOKEN_URL = "https://api.instagram.com/oauth/access_token";
    private static final String API_URL = "https://api.instagram.com/v1";
}
